--set indetation to 2 and use spaces on all FileType
vim.cmd [[
  autocmd FileType * setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
]]
vim.opt.relativenumber = true
vim.opt.swapfile = false
vim.g.base46_cache = vim.fn.stdpath "data" .. "/nvchad/base46/"
vim.g.mapleader = " "
-- bootstrap lazy and all plugins
local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
  local repo = "https://github.com/folke/lazy.nvim.git"
  vim.fn.system { "git", "clone", "--filter=blob:none", repo, "--branch=stable", lazypath }
end

vim.opt.rtp:prepend(lazypath)

local lazy_config = require "configs.lazy"

-- load plugins
require("lazy").setup({
  {
    "NvChad/NvChad",
    lazy = false,
    branch = "v2.5",
    import = "nvchad.plugins",
    config = function()
      require "options"
    end,
  },

  { import = "plugins" },
}, lazy_config)

-- load theme
dofile(vim.g.base46_cache .. "defaults")
dofile(vim.g.base46_cache .. "statusline")

require "nvchad.autocmds"

vim.schedule(function()
  require "mappings"
end)

if vim.fn.filereadable(vim.fn.getcwd() .. "/project.godot") == 1 then
  local pipe = "/tmp/godot.pipe"
  vim.api.nvim_command('echo serverstart("' .. pipe .. '")')
end
