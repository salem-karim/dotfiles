return {
  "nvim-treesitter/nvim-treesitter",
  opts = {
    ensure_installed = {
      "vim",
      "lua",
      "vimdoc",
      "bash",
      -- Web Developement
      "html",
      "css",
      "javascript",
      "typescript",
      "json",
      -- C/C++ and Others
      "c",
      "cpp",
      "cmake",
      "python",
      "rust",
      "go",
      "java",
      "c_sharp",
      "gdscript",
    },
  },
}
