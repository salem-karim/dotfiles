require "nvchad.mappings"

-- add yours here

local map = vim.keymap.set
-- local nomap = vim.keymap.del

map("n", ";", ":", { desc = "CMD enter command mode" })
map("n", ",", ":", { desc = "CMD enter command mode" })
map("i", "jk", "<ESC>")
map("n", "<leader>ll", ":Lazy<cr>", { desc = "Lazy", silent = true })

map("v", "<C-c>", '"+y', { desc = "Copy to System Clipboard" })
map("n", "<C-q>", ":q<CR>", { desc = "Quit", silent = true })
map({ "n", "i", "v" }, "<C-s>", "<cmd> w <cr>")
map({ "n", "i" }, "<C-a>", "ggVG", { desc = "Select All" })
-- map("n", "<leader>SR", function()
--   require("spectre").open()
-- end, { desc = "Replace in Files (Spectre)" })

-- Mappings to move a line or block up and down with out the move plugin (using alt + j/k)

map("n", "<A-j>", ":m .+1<CR>==", { desc = "Move Line Down", silent = true })
map("n", "<A-k>", ":m .-2<CR>==", { desc = "Move Line Up", silent = true })
map("v", "<A-j>", ":m '>+1<CR>gv=gv", { desc = "Move Line Down", silent = true })
map("v", "<A-k>", ":m '<-2<CR>gv=gv", { desc = "Move Line Up", silent = true })

-- CMake Tools & CPP Tools
map("n", "<leader>cc", ":CMakeClean<cr>", { desc = "CMake Clean", silent = true })
map("n", "<leader>cg", ":CMakeGenerate<cr>", { desc = "CMake Generate", silent = true })
map("n", "<leader>cb", ":CMakeBuild<cr>", { desc = "CMake Build", silent = true })
map("n", "<leader>cr", ":CMakeRun<cr>", { desc = "CMake Run", silent = true })
map("n", "<leader>cd", ":CMakeDebug<cr>", { desc = "CMake Debug", silent = true })
map("n", "<leader>ct", ":CMakeRunTest<cr>", { desc = "CMake Run Test", silent = true })
map("n", "<leader>csr", ":CMakeSelectLaunchTarget<cr>", { desc = "CMake Select Run Tareget", silent = true })
map("n", "<leader>csb", ":CMakeSelectBuildTarget<cr>", { desc = "CMake Select Build Target", silent = true })
map("n", "<leader>csc", ":CMakeSelectCwd<cr>", { desc = "CMake Select CWD", silent = true })
vim.api.nvim_set_keymap(
  "v",
  "<leader>im",
  [[<cmd>lua require("configs.cpptools.cpptools").create_func_def()<CR>]],
  { desc = "Create Function Def", silent = true }
)
vim.api.nvim_set_keymap(
  "n",
  "<leader>im",
  [[<cmd>lua require("configs.cpptools.cpptools").create_func_def()<CR>]],
  { desc = "Create Function Def", silent = true }
)
vim.api.nvim_set_keymap(
  "n",
  "<leader>if",
  [[<cmd>lua require("configs.cpptools.cpptools").create_file()<CR>]],
  { desc = "Create File", silent = true }
)

-- DAP
map("n", "<leader>db", ":DapToggleBreakpoint<cr>", { desc = "Toggle Breakpoint", silent = true })
map("n", "<leader>dr", ":DapContinue<cr>", { desc = "Start or Continue Debugger", silent = true })
-- DAP PY
map("n", "<leader>dpr", function()
  require("dap-python").test_method()
end, { desc = "Debug Test Method", silent = true })

-- LazyGit
map("n", "<leader>lg", ":LazyGit<cr>", { desc = "LazyGit", silent = true })

-- Live Server
map("n", "<leader>ls", ":LiveServer<cr>", { desc = "Live Server", silent = true })

-- Dissmiss Noice
map("n", "<leader>dn", ":NoiceDismiss<cr>", { desc = "Dissmiss Notification", silent = true })

--Trouble Nvim
map(
  "n",
  "<leader>dx",
  ":TroubleToggle document_diagnostics<cr>",
  { desc = "Document Diagnostics (Trouble)", silent = true }
)
map(
  "n",
  "<leader>dX",
  ":TroubleToggle workspace_diagnostics<cr>",
  { desc = "Workspace Diagnostics (Trouble)", silent = true }
)
