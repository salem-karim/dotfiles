-- This file  needs to have same structure as nvconfig.lua
-- https://github.com/NvChad/NvChad/blob/v2.5/lua/nvconfig.lua

---@type ChadrcConfig
local M = {}

M.base46 = {
  theme = "catppuccin",
  transparency = true,
  statusline = {
    theme = "minimal",
  },
  hl_override = {
    ["@operator"] = {
      italic = true,
    },
    ["@property.css"] = {
      fg = "purple",
    },
    ["@tag.delimiter"] = {
      fg = "vibrant_green",
    },
    ["@tag"] = {
      fg = "blue",
    },
    ["@variable.member"] = {
      fg = "cyan",
    },
    ["@variable.parameter"] = {
      fg = "teal",
    },
    ["@keyword.function"] = {
      fg = "dark_purple",
    },
    ["@variable"] = {
      fg = "mylavender",
    },
    ["@type.builtin"] = {
      fg = "sapphire",
    },
    ["@keyword"] = {
      fg = "sapphire",
    },
    Type = {
      fg = "mauve",
    },
    Comment = {
      fg = "#aaaaaa",
      bold = false,
      italic = true,
    },
    ["@comment"] = {
      fg = "#aaaaaa",
      bold = false,
      italic = true,
    },
    NvDashAscii = {
      bg = "none",
      fg = "blue",
    },
    NvDashButtons = {
      bg = "none",
      fg = "light_grey",
    },
  },
  nvdash = {
    load_on_startup = true,
    header = {
      " ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗",
      " ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║",
      " ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║",
      " ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║",
      " ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║",
      " ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝",
      -- "                      ▄██▄                      ",
      -- "                    ▄██████▄                    ",
      -- "                  ▄██████████▄                  ",
      -- "                ▄██████████████▄                ",
      -- "              ▄██████████████████▄              ",
      -- "            ▄██████████████████████▄            ",
      -- "          ▄██▄                    ▄██▄          ",
      -- "        ▄██████▄                ▄██████▄        ",
      -- "      ▄██████████▄            ▄██████████▄      ",
      -- "    ▄██████████████▄        ▄██████████████▄    ",
      -- "  ▄██████████████████▄    ▄██████████████████▄  ",
      -- "▄██████████████████████▄▄██████████████████████▄",
    },
    buttons = {
      { "  Find File", "Spc f f", "Telescope find_files" },
      { "󰈚  Recent Files", "Spc f o", "Telescope oldfiles" },
      { "󰈭  Find Word", "Spc f w", "Telescope live_grep" },
      { "  Bookmarks", "Spc m a", "Telescope marks" },
      { "  Themes", "Spc t h", "Telescope themes" },
      { "  Mappings", "Spc c h", "NvCheatsheet" },
    },
  },
}

return M
