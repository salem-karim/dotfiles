local dap = require "dap"
local lldb_dap = {
  name = "Launch",
  type = "lldb",
  request = "launch",
  program = function()
    return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
  end,
  cwd = "${workspaceFolder}",
  stopOnEntry = false,
  args = {},
}

dap.adapters["pwa-node"] = {
  type = "server",
  host = "127.0.0.1",
  port = 8123,
  executable = {
    commmand = "js-debug-adapter",
  },
}

for _, language in ipairs { "typescript", "javascript" } do
  dap.configurations[language] = {
    {
      type = "pwa-node",
      request = "launch",
      name = "Launch file",
      program = "${file}",
      cwd = "${workspaceFolder}",
      runtimeExecutable = "node",
    },
  }
end

-- GDScript config
dap.adapters.godot = {
  type = "server",
  host = "127.0.0.1",
  port = 6006,
}

dap.configurations.gdscript = {
  {
    type = "godot",
    request = "launch", -- either "launch" or "attach"
    name = "Launch Main Scene",
    -- specific to gdscript
    project = "${workspaceFolder}",
  },
}

-- C/C++, Rust config
dap.adapters.lldb = {
  type = "executable",
  command = "/home/karim/.nix-profile/bin/lldb-dap",
  name = "lldb",
}

dap.configurations.c = lldb_dap
dap.configurations.cpp = lldb_dap
