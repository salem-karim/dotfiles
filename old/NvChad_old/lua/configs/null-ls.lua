local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
local null_ls = require "null-ls"

local opts = {
  sources = {
    -- Python
    null_ls.builtins.diagnostics.mypy,
    -- C++
    null_ls.builtins.diagnostics.cppcheck.with {
      extra_args = { "--std" },
    },
    null_ls.builtins.diagnostics.cmake_lint,
    -- Gdscript
    null_ls.builtins.diagnostics.gdlint,
    null_ls.builtins.formatting.gdformat.with {
      extra_args = { "--use-spaces=2" },
    },
    require "none-ls.diagnostics.eslint_d",
  },
  on_attach = function(client, bufnr)
    if client.supports_method "textDocument/formatting" then
      vim.api.nvim_clear_autocmds {
        group = augroup,
        buffer = bufnr,
      }
      vim.api.nvim_create_autocmd("BufWritePre", {
        group = augroup,
        buffer = bufnr,
        callback = function()
          vim.lsp.buf.format { bufnr = bufnr }
        end,
      })
    end
  end,
}

return opts
