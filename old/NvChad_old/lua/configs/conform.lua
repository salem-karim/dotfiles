local options = {
  formatters_by_ft = {
    lua = { "stylua" },
    css = { "prettier" },
    html = { "prettier" },
    javascript = { "prettier" },
    typescript = { "prettier" },
    python = { "yapf" },
    c = { "clang-format" },
    cpp = { "clang-format" },
    cmake = { "cmake_format" },
  },
  formatters = {
    yapf = {
      args = { "--style={indent_width:2}" },
    },
  },
  format_on_save = {
    -- These options will be passed to conform.format()
    timeout_ms = 500,
    lsp_fallback = true,
  },
}

require("conform").setup(options)
