return {
  "mfussenegger/nvim-lint",
  opts = {
    events = { "BufWritePost", "BufReadPost", "InsertLeave" },
    linters_by_ft = {
      python = { "ruff" },
      gdscript = { "gdlint" },
      cmake = { "cmake-lint" },
    },
  },
}
