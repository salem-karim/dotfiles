return {
  "stevearc/conform.nvim",
  event = "BufWritePre",
  opts = {
    formatters_by_ft = {
      lua = { "stylua" },
      css = { "prettier" },
      html = { "prettier" },
      javascript = { "prettier" },
      typescript = { "prettier" },
      python = { "yapf" },
      cmake = { "cmake_format" },
      gdscript = { "gdformat" },
    },
    formatters = {
      yapf = {
        args = { "--style={indent_width:2}" },
      },
    },
  },
}
