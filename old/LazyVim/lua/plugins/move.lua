return {
  "fedepujol/move.nvim",
  opts = {
    keys = {
      { "<A-j>", "<cmd>MoveLine(1)<CR>", desc = "Move Line down" },
      { "<A-k>", "<cmd>MoveLine(-1)<CR>", desc = "Move Line up" },
      { "v", "<A-j>", "<cmd>MoveBlock(1)<CR>", desc = "Move Block down" },
      { "v", "<A-k>", "<cmd>MoveBlock(-1)<CR>", desc = "Move Block up" },
    },
  },
}
