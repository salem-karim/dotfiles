return {
  {
    "akinsho/toggleterm.nvim",
    cmd = "ToggleTerm",
    build = ":ToggleTerm",
    keys = {
      { "<A-h>", "<cmd>ToggleTerm dir=vim.loop.cwd() direction=horizontal<cr>", desc = "Toggle horizontal terminal" },
      { "<A-i>", "<cmd>ToggleTerm dir=vim.loop.cwd() direction=float<cr>", desc = "Toggle floating terminal" },
    },
    opts = {
      terminal_mappings = true,
      start_in_insert = true,
      close_on_exit = true,
    },
  },
}
