return {
  "aurum77/live-server.nvim",
  lazy = false,
  config = function()
    local status_ok, live_server = pcall(require, "live_server")
    if not status_ok then
      return
    end

    live_server.setup({
      port = 5050,
      browser_command = "firefox", -- Command or executable path
      quiet = false,
      no_css_inject = true,
      install_path = os.getenv("HOME") .. "/live/",
    })
  end,
  cmd = {
    "LiveServer",
    "LiveServerStart",
    "LiveServerStop",
  },
  build = function()
    require("liver_server.util").install()
  end,
  keys = {
    { "<leader>ls", ":LiveServer<cr>", { desc = "Start Live Server", silent = true } },
  },
}
