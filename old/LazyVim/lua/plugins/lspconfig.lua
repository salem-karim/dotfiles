local function organize_imports()
  local params = {
    command = "_typescript.organizeImports",
    arguments = { vim.api.nvim_buf_get_name(0) },
  }
  vim.lsp.bug.execute_command(params)
end

local myinit = function(client, _)
  if client.supports_method("textDocument/semanticTokens") then
    client.server_capabilities.semanticTokensProvider = nil
  end
end

return {
  "neovim/nvim-lspconfig",
  opts = {
    servers = {
      pyright = {},
      tsserver = {
        init_options = {
          disableSuggestions = true,
        },
        command = {
          OrganizeImports = {
            organize_imports,
            description = "Organize Imports",
          },
        },
      },

      html = {},
      cssls = {},
      cmake = {},
      eslint = {},
      gdscript = {},
    },
    -- setup = {
    --   gdscript = {
    --     require("lspconfig").gdscript.setup({
    --       on_init = myinit,
    --     }),
    --   },
    -- },
  },
}
