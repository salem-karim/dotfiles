return {
  "williamboman/mason.nvim",
  opts = {
    ensure_installed = {
      "lua-language-server",
      "stylua",
      -- "gdtoolkit",
      -- Web Developement
      "html-lsp",
      "css-lsp",
      "prettier",
      "typescript-language-server",
      "eslint-lsp",
      "js-debug-adapter",
      --C/C++
      "clangd",
      "clang-format",
      "codelldb",
      "cmakelang",
      "cmake-language-server",
      "cpplint",
      --Python
      "pyright",
      "mypy",
      "ruff",
      "ruff-lsp",
      "yapf",
      "debugpy",
    },
  },
}
