-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local map = vim.keymap.set
local nomap = vim.keymap.del
local Util = require("lazyvim.util")

nomap("n", "<leader>L")
nomap("n", "<leader>l")
nomap("n", "<leader>gg")

map("n", "<leader>lg", function()
  Util.terminal({ "lazygit" }, { esc_esc = false, ctrl_hjkl = false })
end, { desc = "Lazygit (cwd)" })

map("n", "<C-space>", ":", { desc = "Enter command mode" })
map("v", "<C-c>", '"+y', { desc = "Copy to System Clipboard" })
map("n", "<C-q>", ":q<CR>", { desc = "Quit", silent = true })
map({ "n", "i", "v" }, "<C-s>", "<cmd> w <cr>")
map({ "n", "i" }, "<C-a>", "ggVG", { desc = "Select All" })
map("t", "<C-c>", "<C-\\><C-N>", { desc = "terminal escape terminal mode" })

-- CMake Tools & CPP Tools
map("n", "<leader>CC", ":CMakeClean<cr>", { desc = "CMake Clean", silent = true })
map("n", "<leader>CG", ":CMakeGenerate<cr>", { desc = "CMake Generate", silent = true })
map("n", "<leader>CB", ":CMakeBuild<cr>", { desc = "CMake Build", silent = true })
map("n", "<leader>CR", ":CMakeRun<cr>", { desc = "CMake Run", silent = true })
map("n", "<leader>CD", ":CMakeDebug<cr>", { desc = "CMake Debug", silent = true })
map("n", "<leader>CT", ":CMakeTargetSettings<cr>", { desc = "CMake Target Settings", silent = true })

map("n", "<leader>ic", ":lua require('cpptools.cpptools').create_file()<cr>", { desc = "Create File", silent = true })
map(
  { "n", "v" },
  "<leader>if",
  ":lua require('cpptools.cpptools').create_func_def()<cr>",
  { desc = "Implement Functions", silent = true }
)

map("n", "<leader>db", ":DapToggleBreakpoint<cr>", { desc = "Toggle Breakpoint", silent = true })
map("n", "<leader>dr", ":DapContinue<cr>", { desc = "Start or Continue Debugger", silent = true })

--DAP PY
map("n", "<leader>dpr", function()
  require("dap-python").test_method()
end, { desc = "Debug Test Method" })

--Overwrite LazyVim Defaults

--Bufferline
nomap("n", "<S-h>")
map("n", "<tab>", ":BufferLineCycleNext<cr>", { desc = "Next Buffer", silent = true })
nomap("n", "<S-l>")
map("n", "<S-tab>", ":BufferLineCyclePrev<cr>", { desc = "Next Buffer", silent = true })
nomap("n", "<leader>bd")
map("n", "<leader>dd", function()
  local bd = require("mini.bufremove").delete
  if vim.bo.modified then
    local choice = vim.fn.confirm(("Save changes to %q?"):format(vim.fn.bufname()), "&Yes\n&No\n&Cancel")
    if choice == 1 then -- Yes
      vim.cmd.write()
      bd(0)
    elseif choice == 2 then -- No
      bd(0, true)
    end
  else
    bd(0)
  end
end, { desc = "Close Buffer", silent = true })

-- NeoTree

nomap("n", "<leader>e")
nomap("n", "<leader>E")
map("n", "<leader>n", function()
  require("neo-tree.command").execute({ toggle = true, dir = vim.loop.cwd() })
end, { desc = "Toggle NeoTree cwd" })

map("n", "<C-n>", function()
  require("neo-tree.command").execute({ toggle = true, dir = Util.root() })
end, { desc = "Toggle NeoTree root" })

-- Telescope
nomap("n", "<leader>ff")
nomap("n", "<leader>fF")
map("n", "<leader>ff", ":Telescope find_files<cr>", { desc = "Find Files" })
