local plugin_path = debug.getinfo(1, "S").source:sub(2)
plugin_path = vim.fn.fnamemodify(plugin_path, ":h")

local function clear_cmd()
  vim.cmd([[normal! :\<C-u>]])
end

local function makedirs(path)
  -- print(path)
  if not path then
    return nil
  end
  vim.fn.mkdir(path)
end

local function isdir(path)
  if not path then
    return nil
  end
  if vim.fn.isdirectory(path) == 1 then
    return true
  else
    return false
  end
end

local function hasfile(path)
  if not path then
    return nil
  end
  local file = io.open(path, "r")
  if file then
    print("File exists: ", path) -- Add this print statement
    file:close()
    return true
  else
    -- print("File does not exist: ", path) -- Add this print statement
    return false
  end
end

local function test()
  print("test")
end

return {
  -- dirlist = dirlist,
  makedirs = makedirs,
  clear_cmd = clear_cmd,
  isdir = isdir,
  hasfile = hasfile,
  test = test,
}
