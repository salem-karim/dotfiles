-- bootstrap lazy.nvim, LazyVim and your plugins
require("config.lazy")
vim.opt.swapfile = false
if vim.fn.filereadable(vim.fn.getcwd() .. "/project.godot") == 1 then
  local pipe = "/tmp/godot.pipe"
  vim.api.nvim_command('echo serverstart("' .. pipe .. '")')
end
vim.cmd([[
  autocmd FileType * setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
]])
