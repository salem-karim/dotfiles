#!/usr/bin/env bash

# CHANGE IF NEEDED:
# - replace with your Terminal Emulator executable
term_exec="kitty"
# - replace with your Neovim executable
# nvim_exec="nvim"
nvim_exec="/run/current-system/sw/bin/nvim"
# - replace with other path for the Neovim server pipe
server_path="/tmp/godot.pipe"

start_server() {
	# "$term_exec" -e "$nvim_exec" --listen "$server_path" "$1"

	"$term_exec" -e "$nvim_exec" "$1"
}

open_file_in_server() {
	# - escape stuff because nvim will not
	"$term_exec" -e "$nvim_exec" --listen "$server_path"  --server "$server_path" --remote-send "<C-\><C-n>:n $1<CR>:call cursor($2)<CR>"
}

if ! [ -e "$server_path" ]; then
	# - start server FIRST then open the file
	start_server "$1"
	open_file_in_server "$1" "$2"
else
	open_file_in_server "$1" "$2"
fi
