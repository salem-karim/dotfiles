{ config, pkgs, lib, ... }:

{
  programs.starship = {
    enable = true;
    enableBashIntegration = false;
    enableZshIntegration = false;
    settings = {
      add_newline = false;

      format = lib.concatStrings [
        "[](bg:#35353d fg:#40a02b)"
        "$username"
        "[](bg:#209fb5 fg:#40a02b)"
        "$directory"
        "[](fg:#209fb5 bg:#e64553)"
        "$git_branch"
        "$git_status"
        "[](fg:#e64553)"
        "$fill"
        "[](#689AB6)"
        "$c"
        "$elixir"
        "$elm"
        "$golang"
        "$gradle"
        "$haskell"
        "$java"
        "$julia"
        "$nodejs"
        "$nim"
        "$rust"
        "$scala"
        "[](fg:#689AB6 bg:#06969A)"
        "$docker_context"
        "[](fg:#06969A bg:#33658A)"
        "$time"
        "[](fg:#33658A)"
        "$line_break"
        "$character"
      ];

      hostname = {
        ssh_only = true;
        style = "bg:#30A27F";
        format = "[ $hostname ]($style)";
        disabled = false;
      };

      fill = {
        symbol = " ";
      };

      username = {
        show_always = true;
        style_user = "bg:#40a02b";
        style_root = "bg:#40a02b";
        format = "[$user ]($style)";
        disabled = false;
      };

      directory = {
        style = "bg:#209fb5";
        read_only = " ";
        format = "[ $path ]($style)";
        truncation_length = 3;
        truncation_symbol = "…/";
        substitutions = {
          Documents = "󰈙 ";
          Downloads = " ";
          Music = " ";
          Pictures = " ";
        };
      };

      character = {
        success_symbol = "[❯](green)";
        error_symbol = "[❯](red)";
        vicmd_symbol = "[❮](purple)";
      };

      c = {
        symbol = " ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      docker_context = {
        symbol = " ";
        style = "bg:#06969A";
        format = "[ $symbol $context ]($style)";
      };

      elixir = {
        symbol = " ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      elm = {
        symbol = " ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      git_branch = {
        symbol = "";
        style = "bg:#e64553";
        format = "[ $branch ]($style)";
      };

      git_status = {
        style = "bg:#e64553";
        format = "[$all_status$ahead_behind ]($style)";
      };

      golang = {
        symbol = " ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      gradle = {
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      haskell = {
        symbol = " ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      java = {
        symbol = " ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      julia = {
        symbol = " ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      nodejs = {
        symbol = "";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      nim = {
        symbol = "󰆥 ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      rust = {
        symbol = "";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      scala = {
        symbol = " ";
        style = "bg:#689AB6";
        format = "[ $symbol ($version) ]($style)";
      };

      time = {
        disabled = false;
        time_format = "%R";  # Hour:Minute Format
        style = "bg:#33658A";
        format = "[ $time ]($style)";
      };
    };
  };
}
