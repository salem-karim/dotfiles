{ ... }:
let
  no-blur = "Brave-browser|thunderbird|kitty|zoom|Vmware|vlc|com.obsproject.Studio|Gimp-2.10|firefox|.*.x86_64|steam_app_\\d+|org.kde.okular|Godot|Cemu|ONLYOFFICE|Unity|flameshot|BambuStudio|blender|Minecraft*|Slay the Spire|com.mitchellh.ghostty|designer.exe|photo.exe|publisher.exe|zen|gamescope|com.oracle.javafx.scenebuilder.app.SceneBuilderApp";
in
{
  programs.plasma = {
    enable = true;
    desktop = {
      mouseActions.rightClick = "contextMenu";
    };
    fonts = {
      general = {
        family = "Noto Sans";
        pointSize = 10;
      };
      fixedWidth = {
        family = "Hack";
        pointSize = 10;
      };
      small = {
        family = "Noto Sans";
        pointSize = 8;
      };
      toolbar = {
        family = "Noto Sans";
        pointSize = 10;
      };
      menu = {
        family = "Noto Sans";
        pointSize = 10;
      };
      windowTitle = {
        family = "Noto Sans";
        pointSize = 10;
      };
    };

    input.keyboard = {
      numlockOnStartup = "on";
      repeatDelay = 600;
      repeatRate = 25.0;
      layouts = [
        {
          layout = "at";
        }
      ];
      switchingPolicy = "global";
    };

    input.mice = [
      {
        name = "Logitech MX Master 3S";
        enable = true;
        leftHanded = false;
        middleButtonEmulation = false;
        acceleration = -0.6;
        accelerationProfile = "none";
        naturalScroll = true;
        vendorId = "046d";
        productId = "b034";
      }
      {
        name = "Logitech USB Receiver Mouse";
        enable = true;
        leftHanded = false;
        middleButtonEmulation = false;
        acceleration = -0.6;
        accelerationProfile = "none";
        naturalScroll = true;
        vendorId = "046d";
        productId = "c548";
      }
    ];

    kscreenlocker = {
      autoLock = true;
      timeout = 5; # Minutes
      lockOnResume = true;
      passwordRequired = true;
      passwordRequiredDelay = 60; # Seconds
      appearance = {
        alwaysShowClock = true;
        showMediaControls = true;
        wallpaper = "/home/karim/Pictures/Wallpapers/sergey-savvin-forest-night.jpg";
      };
    };

    kwin = {
      borderlessMaximizedWindows = true;
      cornerBarrier = true;
      edgeBarrier = 100;
      effects = {
        blur.enable = false;
        cube.enable = false;
        desktopSwitching.animation = "slide";
        dimAdminMode.enable = false;
        dimInactive.enable = false;
        fallApart.enable = false;
        fps.enable = false;
        minimization.animation = "magiclamp";
        shakeCursor.enable = true;
        slideBack.enable = false;
        snapHelper.enable = false;
        translucency.enable = false;
        windowOpenClose.animation = "scale";
        wobblyWindows.enable = true;
      };

      nightLight = {
        temperature.day = 6500;
        temperature.night = 4000;
        enable = true;
        mode = "location";
        location = {
          latitude = "48.20817430";
          longitude = "16.37381890";
        };
      };

      titlebarButtons.left = [
        "more-window-actions"
        "on-all-desktops"
      ];
      titlebarButtons.right = [
        "minimize"
        "maximize"
        "close"
      ];
      virtualDesktops = {
        number = 5;
        rows = 1;
        names = [
          "Desktop 1"
          "Desktop 2"
          "Desktop 3"
          "Desktop 4"
          "Desktop 5"
        ];
      };
    };

    panels = [
      {
        height = 38;
        maxLength = 1790;
        lengthMode = "custom";
        location = "top";
        alignment = "center";
        hiding = "none";
        floating = true;
        opacity = "translucent";
        screen = [
          0
          1
        ];
        widgets = [
          "org.dhruv8sh.kara"
          # {
          #   panelSpacer = {
          #     expanding = true;
          #   };
          # }
          "luisbocanegra.panelspacer.extended"
          {
            digitalClock = {
              calendar = {
                firstDayOfWeek = "monday";
                plugins = [
                  "holidaysevents"
                  "pimevents"
                ];
              };
              settings = {

              };
            };
          }
          # {
          #   panelSpacer = {
          #     expanding = true;
          #   };
          # }
          "luisbocanegra.panelspacer.extended"
          {
            systemTray = {
              items = {
                showAll = false;
                shown = [
                  "org.kde.plasma.battery"
                  "org.kde.plasma.volume"
                  "org.kde.plasma.keyboardlayout"
                  "org.kde.plasma.networkmanagement"
                ];
                hidden = [
                  "org.kde.plasma.devicenotifier"
                  "org.kde.plasma.keyboardlayout"
                  "org.kde.plasma.printmanager"
                  "org.kde.plasma.clipboard"
                  "org.kde.kscreen"
                ];
              };
            };
          }
          "luisbocanegra.panel.colorizer"
          "org.kde.plasma.shutdownorswitch"
        ];
      }
      {
        height = 44;
        lengthMode = "fit";
        location = "bottom";
        alignment = "center";
        hiding = "autohide";
        floating = true;
        opacity = "translucent";
        screen = [
          0
          1
        ];
        widgets = [
          {
            kickoff = {
              icon = "nix-snowflake";
              showButtonsFor = "powerAndSession";
              showActionButtonCaptions = false;
            };
          }
          {
            iconTasks = {
              launchers = [
                "preferred://filemanager"
                "applications:kitty.desktop"
                "preferred://browser"
                "applications:thunderbird.desktop"
                "applications:com.rtosta.zapzap.desktop"
                "applications:vesktop.desktop"
                "applications:steam.desktop"
                "applications:idea-ultimate.desktop"
                "applications:onlyoffice-desktopeditors.desktop"
                "applications:1password.desktop"
                "applications:systemsettings.desktop"
              ];
              appearance = {
                showTooltips = true;
                highlightWindows = true;
                indicateAudioStreams = true;
                fill = true;
                rows = {
                  multirowView = "never";
                };
                iconSpacing = "medium";
              };
              behavior = {
                grouping.method = "byProgramName";
                minimizeActiveTaskOnClick = true;
                middleClickAction = "newInstance";
                showTasks = {
                  onlyInCurrentScreen = true;
                  onlyInCurrentDesktop = false;
                  onlyInCurrentActivity = false;
                  onlyMinimized = false;
                };
                unhideOnAttentionNeeded = true;
                newTasksAppearOn = "right";
              };
            };
          }
          "luisbocanegra.panel.colorizer"
          # gtk3.extraConfig = {
          #   gtk-application-prefer-dark-theme = true;
          # };
          #
          # gtk4.extraConfig = {
          #   gtk-application-prefer-dark-theme = true;
          # };
        ];
      }
    ];

    spectacle.shortcuts = {
      captureActiveWindow = "Meta+Print";
      captureCurrentMonitor = "";
      captureEntireDesktop = "Shift+Print";
      captureRectangularRegion = "Meta+Shift+Print";
      captureWindowUnderCursor = "Meta+Ctrl+Print";
      launch = [
        "Meta+Shift+S"
        "Print"
      ];
      launchWithoutCapturing = "";
      recordRegion = [
        "Meta+R"
        "Meta+Shift+R"
      ];
      recordScreen = "Meta+Alt+R";
      recordWindow = "Meta+Ctrl+R";
    };

    workspace = {
      clickItemTo = "select";
      colorScheme = "CatppuccinMochaTeal";
      cursor = {
        size = 32;
        theme = "Catppuccin-Mocha-Dark";
      };
      iconTheme = "Papirus-Dark";
      soundTheme = "Ocena";
      theme = "default";
      # lookAndFeel = "Catppuccin-Mocha-Teal";
      wallpaper = "/home/karim/Pictures/Wallpapers/sergey-savvin-forest-night.jpg";
      splashScreen.theme = "Catppuccin-Mocha-Teal";
      windowDecorations = {
        library = "org.kde.kwin.aurorae";
        theme = "__aurorae__svg__CatppuccinMocha-Classic";
      };
    };

    shortcuts = {
      # "KDE Keyboard Layout Switcher"."Switch to Next Keyboard Layout" = "Meta+Alt+K";
      "kcm_touchpad"."Toggle Touchpad" = "Meta+Ctrl+Shift+F12";
      "kmix"."mic_mute" = [
        "Meta+Volume Mute"
        "Microphone Mute"
      ];
      "ksmserver"."Lock Session" = [
        "Screensaver"
        "Meta+F7"
      ];
      "ksmserver"."Log Out" = "Ctrl+Alt+Del";
      "kwin"."Edit Tiles" = "Meta+T";
      "kwin"."Expose" = "Ctrl+F9";
      "kwin"."Grid View" = "Meta+G";
      "plasmashell"."manage activities" = "none";
      "kwin"."Window Close" = "Meta+Q";
      "kwin"."Kill Window" = "Meta+W";
      "kwin"."KrohnkiteIncrease" = "Meta+Ctrl+Shift+W";
      "kwin"."KrohnkiteDecrease" = "Meta+Ctrl+Shift+S";
      "kwin"."KrohnkiteFloatAll" = "Meta+Shift+F";
      "kwin"."KrohnkiteFocusUp" = "Meta+K";
      "kwin"."KrohnkiteFocusDown" = "Meta+J";
      "kwin"."KrohnkiteFocusLeft" = "Meta+H";
      "kwin"."KrohnkiteFocusRight" = "Meta+L";
      "kwin"."KrohnkiteMonocleLayout" = "Meta+M";
      "kwin"."KrohnkiteBTreeLayout" = "Meta+Ctrl+Shift+B";
      "kwin"."KrohnkiteQuaterLayout" = "Meta+Ctrl+Shift+Q";
      "kwin"."KrohnkiteNextLayout" = "Meta+#";
      "kwin"."KrohnkitePreviousLayout" = "Meta+|";
      "kwin"."KrohnkiteSetMaster" = "Meta+Return";
      "kwin"."KrohnkiteShiftLeft" = "Meta+Ctrl+H";
      "kwin"."KrohnkiteShiftDown" = "Meta+Ctrl+J";
      "kwin"."KrohnkiteShiftUp" = "Meta+Ctrl+K";
      "kwin"."KrohnkiteShiftRight" = "Meta+Ctrl+L";
      "kwin"."KrohnkiteShrinkHeight" = "Alt+Shift+W";
      "kwin"."KrohnkiteShrinkWidth" = "Alt+Shift+A";
      "kwin"."KrohnkiteGrowHeight" = "Alt+Shift+S";
      "kwin"."KrohnkitegrowWidth" = "Alt+Shift+D";
      "kwin"."KrohnkiteToggleFloat" = "Meta+X";
      "kwin"."MoveMouseToCenter" = "Meta+F6";
      "kwin"."MoveMouseToFocus" = "Meta+F5";
      "kwin"."Overview" = "Meta+A";
      "kwin"."Show Desktop" = "Meta+D";
      "kwin"."Switch One Desktop Down" = "Meta+Ctrl+Down";
      "kwin"."Switch One Desktop Up" = "Meta+Ctrl+Up";
      "kwin"."Switch One Desktop to the Left" = [
        "Meta+Ctrl+A"
        "Meta+Ctrl+Left"
      ];
      "kwin"."Switch One Desktop to the Right" = [
        "Meta+Ctrl+D"
        "Meta+Ctrl+Right"
      ];
      "kwin"."Switch Window Up" = "Meta+Alt+Up";
      "kwin"."Switch Window Down" = "Meta+Alt+Down";
      "kwin"."Switch Window Left" = "Meta+Alt+Left";
      "kwin"."Switch Window Right" = "Meta+Alt+Right";
      "kwin"."Switch to Desktop 1" = "Meta+1";
      "kwin"."Switch to Desktop 2" = "Meta+2";
      "kwin"."Switch to Desktop 3" = "Meta+3";
      "kwin"."Switch to Desktop 4" = "Meta+4";
      "kwin"."Switch to Desktop 5" = "Meta+5";
      # "kwin"."Switch to Screen 0" = "none,,Switch to Screen 0";
      # "kwin"."Switch to Screen 1" = "none,,Switch to Screen 1";
      "kwin"."Switch to Screen to the Left" = "Alt+H";
      "kwin"."Switch to Screen to the Right" = "Alt+L";
      "kwin"."Walk Through Windows" = "Alt+Tab";
      "kwin"."Walk Through Windows (Reverse)" = "Alt+Shift+Tab";
      "kwin"."Window Fullscreen" = "F11";
      "kwin"."Window Maximize" = [
        "Meta+PgUp"
        "Meta+Shift+K"
        "Meta+Shift+Up"
      ];
      "kwin"."Window Minimize" = [
        "Meta+Shift+J"
        "Meta+PgDown"
      ];
      "kwin"."Window One Desktop Down" = "Meta+Ctrl+Shift+Down";
      "kwin"."Window One Desktop Up" = "Meta+Ctrl+Shift+Up";
      "kwin"."Window One Desktop to the Left" = [
        "Meta+Alt+H"
        "Meta+Ctrl+Shift+Left"
      ];
      "kwin"."Window One Desktop to the Right" = [
        "Meta+Alt+L"
        "Meta+Ctrl+Shift+Right"
      ];
      "kwin"."Window One Screen to the Left" = [
        "Meta+Ctrl+Shift+Left"
        "Meta+Shift+H"
      ];
      "kwin"."Window One Screen to the Right" = [
        "Meta+Ctrl+Shift+Right"
        "Meta+Shift+L"
      ];
      "kwin"."Window Operations Menu" = "Alt+F3";
      "kwin"."Window Pack Down" = "none,,Move Window Down";
      "kwin"."Window Pack Left" = "none,,Move Window Left";
      "kwin"."Window Pack Right" = "none,,Move Window Right";
      "kwin"."Window Pack Up" = "none,,Move Window Up";
      "kwin"."Window Quick Tile Bottom" = "Meta+Down";
      "kwin"."Window Quick Tile Left" = "Meta+Left";
      "kwin"."Window Quick Tile Right" = "Meta+Right";
      "kwin"."Window Quick Tile Top" = "Meta+Up";
      "kwin"."Window to Screen 0" = "none,,Move Window to Screen 0";
      "kwin"."Window to Screen 1" = "none,,Move Window to Screen 1";
      "org_kde_powerdevil"."powerProfile" = "Meta+Shift+B";
      "plasmashell"."activate application launcher" = [
        "Alt+F1"
        "Meta+Ctrl+Space"
      ];
      "plasmashell"."clipboard_action" = "Meta+Ctrl+X";
      "plasmashell"."show dashboard" = "Ctrl+F12";
      # "services/firefox.desktop"."_launch" = "Meta+B";
      "services/zen.desktop"."_launch" = "Meta+B";
      "services/kitty.desktop"."_launch" = "Meta+C";
      # "services/yazi.desktop"."_launch" = "Meta+F";
      "services/org.flameshot.Flameshot.desktop"."Capture" = "Meta+S";
      "ActivityManager"."switch-to-activity-7f2af706-f9e4-4173-b92b-6bbb4e0e332c" = [ ];
      "KDE Keyboard Layout Switcher"."Switch to Last-Used Keyboard Layout" = "none";
      "KDE Keyboard Layout Switcher"."Switch to Next Keyboard Layout" = "none";
      "kaccess"."Toggle Screen Reader On and Off" = "Meta+Alt+S";
      "kmix"."decrease_microphone_volume" = "Microphone Volume Down";
      "kmix"."decrease_volume" = "Volume Down";
      "kmix"."decrease_volume_small" = "Shift+Volume Down";
      "kmix"."increase_microphone_volume" = "Microphone Volume Up";
      "kmix"."increase_volume" = "Volume Up";
      "kmix"."increase_volume_small" = "Shift+Volume Up";
      "kmix"."mute" = "Volume Mute";
      "kwin"."Activate Window Demanding Attention" = "none";
      "kwin"."ExposeAll" = [
        "Ctrl+F10"
        "Launch (C),Ctrl+F10"
        "Launch (C),Toggle Present Windows (All desktops)"
      ];
      "kwin"."ExposeClass" = "Ctrl+F7";
      "kwin"."KrohnkiteFocusPrev" = "Meta+\\";
      "kwin"."Walk Through Windows of Current Application" = "Alt+`";
      "kwin"."Walk Through Windows of Current Application (Reverse)" = "Alt+~";
      "kwin"."Window to Next Screen" = "Meta+Shift+Right";
      "kwin"."Window to Previous Desktop" = "none,,Window to Previous Desktop";
      "kwin"."Window to Previous Screen" = "Meta+Shift+Left";
      "kwin"."view_actual_size" = "Meta+0";
      "kwin"."view_zoom_in" = [
        "Meta++"
        "Meta+=,Meta++"
        "Meta+=,Zoom In"
      ];
      "kwin"."view_zoom_out" = "Meta+-";
      "mediacontrol"."nextmedia" = "Media Next";
      "mediacontrol"."pausemedia" = "Media Pause";
      "mediacontrol"."playpausemedia" = "Media Play";
      "mediacontrol"."previousmedia" = "Media Previous";
      "mediacontrol"."stopmedia" = "Media Stop";
      "org_kde_powerdevil"."Decrease Keyboard Brightness" = "Keyboard Brightness Down";
      "org_kde_powerdevil"."Decrease Screen Brightness" = "Monitor Brightness Down";
      "org_kde_powerdevil"."Decrease Screen Brightness Small" = "Shift+Monitor Brightness Down";
      "org_kde_powerdevil"."Hibernate" = "Hibernate";
      "org_kde_powerdevil"."Increase Keyboard Brightness" = "Keyboard Brightness Up";
      "org_kde_powerdevil"."Increase Screen Brightness" = "Monitor Brightness Up";
      "org_kde_powerdevil"."Increase Screen Brightness Small" = "Shift+Monitor Brightness Up";
      "org_kde_powerdevil"."PowerDown" = "Power Down";
      "org_kde_powerdevil"."PowerOff" = "Power Off";
      "org_kde_powerdevil"."Sleep" = "Sleep";
      "org_kde_powerdevil"."Toggle Keyboard Backlight" = "Keyboard Light On/Off";
      "plasmashell"."activate task manager entry 1" = "none,Meta+1";
      "plasmashell"."activate task manager entry 2" = "none,Meta+2";
      "plasmashell"."activate task manager entry 3" = "none,Meta+3";
      "plasmashell"."activate task manager entry 4" = "none,Meta+4";
      "plasmashell"."activate task manager entry 5" = "none,Meta+5";
      "plasmashell"."activate task manager entry 6" = "none,Meta+6";
      "plasmashell"."activate task manager entry 7" = "none,Meta+7";
      "plasmashell"."activate task manager entry 8" = "none,Meta+8";
      "plasmashell"."activate task manager entry 9" = "none,Meta+9";
      "plasmashell"."activate task manager entry 10" = "none,Meta+0";
      "plasmashell"."cycle-panels" = "Meta+Alt+P";
      "plasmashell"."next activity" = "none";
      "plasmashell"."previous activity" = "none";
      "plasmashell"."repeat_action" = "none,Meta+Ctrl+R";
      "plasmashell"."show-on-mouse-pos" = "Meta+V";
      "plasmashell"."stop current activity" = "Meta+S";
      "services/org.kde.spectacle.desktop"."RecordRegion" = [
        "Meta+R"
        "Meta+Shift+R"
      ];
      "services/org.kde.spectacle.desktop"."_launch" = [
        "Meta+Shift+S"
        "Print"
      ];
    };

    configFile = {
      "baloofilerc"."General"."dbVersion" = 2;
      "baloofilerc"."General"."exclude filters" =
        "*~,*.part,*.o,*.la,*.lo,*.loT,*.moc,moc_*.cpp,qrc_*.cpp,ui_*.h,cmake_install.cmake,CMakeCache.txt,CTestTestfile.cmake,libtool,config.status,confdefs.h,autom4te,conftest,confstat,Makefile.am,*.gcode,.ninja_deps,.ninja_log,build.ninja,*.csproj,*.m4,*.rej,*.gmo,*.pc,*.omf,*.aux,*.tmp,*.po,*.vm*,*.nvram,*.rcore,*.swp,*.swap,lzo,litmain.sh,*.orig,.histfile.*,.xsession-errors*,*.map,*.so,*.a,*.db,*.qrc,*.ini,*.init,*.img,*.vdi,*.vbox*,vbox.log,*.qcow2,*.vmdk,*.vhd,*.vhdx,*.sql,*.sql.gz,*.ytdl,*.tfstate*,*.class,*.pyc,*.pyo,*.elc,*.qmlc,*.jsc,*.fastq,*.fq,*.gb,*.fasta,*.fna,*.gbff,*.faa,po,CVS,.svn,.git,_darcs,.bzr,.hg,CMakeFiles,CMakeTmp,CMakeTmpQmake,.moc,.obj,.pch,.uic,.npm,.yarn,.yarn-cache,__pycache__,node_modules,node_packages,nbproject,.terraform,.venv,venv,core-dumps,lost+found";
      "baloofilerc"."General"."exclude filters version" = 9;
      "dolphinrc"."General"."ViewPropsTimestamp" = "2024,8,11,21,14,51.971";
      "dolphinrc"."KFileDialog Settings"."Places Icons Auto-resize" = false;
      "dolphinrc"."KFileDialog Settings"."Places Icons Static Size" = 22;
      "dolphinrc"."PreviewSettings"."Plugins" =
        "cursorthumbnail,ebookthumbnail,svgthumbnail,jpegthumbnail,imagethumbnail,opendocumentthumbnail,windowsimagethumbnail,directorythumbnail,audiothumbnail,comicbookthumbnail,djvuthumbnail,exrthumbnail,kraorathumbnail,appimagethumbnail,windowsexethumbnail,fontthumbnail,mobithumbnail,blenderthumbnail,ffmpegthumbs,gsthumbnail,rawthumbnail";
      "kactivitymanagerdrc"."activities"."7f2af706-f9e4-4173-b92b-6bbb4e0e332c" = "Default";
      "kactivitymanagerdrc"."main"."currentActivity" = "7f2af706-f9e4-4173-b92b-6bbb4e0e332c";
      "kded5rc"."Module-browserintegrationreminder"."autoload" = false;
      "kded5rc"."Module-device_automounter"."autoload" = false;
      "kdeglobals"."DirSelect Dialog"."DirSelectDialog Size" = "817,554";
      "kdeglobals"."General"."LastUsedCustomAccentColor" = "146,110,228";
      "kdeglobals"."General"."TerminalApplication" = "kitty";
      "kdeglobals"."General"."TerminalService" = "kitty.desktop";
      "kdeglobals"."KDE"."ShowDeleteCommand" = false;
      "kdeglobals"."KDE"."SingleClick" = false;
      "kdeglobals"."KFileDialog Settings"."Allow Expansion" = false;
      "kdeglobals"."KFileDialog Settings"."Automatically select filename extension" = true;
      "kdeglobals"."KFileDialog Settings"."Breadcrumb Navigation" = false;
      "kdeglobals"."KFileDialog Settings"."Decoration position" = 2;
      "kdeglobals"."KFileDialog Settings"."LocationCombo Completionmode" = 5;
      "kdeglobals"."KFileDialog Settings"."PathCombo Completionmode" = 5;
      "kdeglobals"."KFileDialog Settings"."Show Bookmarks" = false;
      "kdeglobals"."KFileDialog Settings"."Show Full Path" = false;
      "kdeglobals"."KFileDialog Settings"."Show Inline Previews" = true;
      "kdeglobals"."KFileDialog Settings"."Show Preview" = false;
      "kdeglobals"."KFileDialog Settings"."Show Speedbar" = true;
      "kdeglobals"."KFileDialog Settings"."Show hidden files" = true;
      "kdeglobals"."KFileDialog Settings"."Sort by" = "Name";
      "kdeglobals"."KFileDialog Settings"."Sort directories first" = true;
      "kdeglobals"."KFileDialog Settings"."Sort hidden files last" = false;
      "kdeglobals"."KFileDialog Settings"."Sort reversed" = true;
      "kdeglobals"."KFileDialog Settings"."Speedbar Width" = 140;
      "kdeglobals"."KFileDialog Settings"."View Style" = "DetailTree";
      "kdeglobals"."KShortcutsDialog Settings"."Dialog Size" = "600,480";
      "kdeglobals"."PreviewSettings"."MaximumRemoteSize" = 0;
      "kdeglobals"."WM"."activeBackground" = "30,30,46";
      "kdeglobals"."WM"."activeBlend" = "205,214,244";
      "kdeglobals"."WM"."activeForeground" = "205,214,244";
      "kdeglobals"."WM"."inactiveBackground" = "17,17,27";
      "kdeglobals"."WM"."inactiveBlend" = "166,173,200";
      "kdeglobals"."WM"."inactiveForeground" = "166,173,200";
      "kiorc"."Confirmations"."ConfirmDelete" = false;
      "kiorc"."Confirmations"."ConfirmEmptyTrash" = false;
      "kiorc"."Confirmations"."ConfirmTrash" = false;
      "kiorc"."Executable scripts"."behaviourOnLaunch" = "alwaysAsk";
      "krunnerrc"."General"."ActivateWhenTypingOnDesktop" = false;
      "krunnerrc"."General"."FreeFloating" = true;
      "krunnerrc"."General"."historyBehavior" = "ImmediateCompletion";
      "krunnerrc"."Plugins"."browserhistoryEnabled" = false;
      "krunnerrc"."Plugins"."browsertabsEnabled" = false;
      "krunnerrc"."Plugins"."krunner_appstreamEnabled" = false;
      "krunnerrc"."Plugins"."krunner_bookmarksrunnerEnabled" = false;
      "krunnerrc"."Plugins"."krunner_kwinEnabled" = false;
      "krunnerrc"."Plugins"."krunner_plasma-desktopEnabled" = false;
      "krunnerrc"."Plugins"."krunner_webshortcutsEnabled" = false;
      "krunnerrc"."Plugins"."org.kde.activities2Enabled" = false;
      "krunnerrc"."Plugins/Favorites"."plugins" = "krunner_services";
      "kservicemenurc"."Show"."compressfileitemaction" = true;
      "kservicemenurc"."Show"."extractfileitemaction" = true;
      "kservicemenurc"."Show"."forgetfileitemaction" = true;
      "kservicemenurc"."Show"."installFont" = true;
      "kservicemenurc"."Show"."kactivitymanagerd_fileitem_linking_plugin" = true;
      "kservicemenurc"."Show"."kio-admin" = true;
      "kservicemenurc"."Show"."makefileactions" = true;
      "kservicemenurc"."Show"."mountisoaction" = true;
      "kservicemenurc"."Show"."runInKonsole" = false;
      "kservicemenurc"."Show"."slideshowfileitemaction" = true;
      "kservicemenurc"."Show"."tagsfileitemaction" = true;
      "kservicemenurc"."Show"."wallpaperfileitemaction" = true;
      "kwalletrc"."Wallet"."First Use" = false;
      # "kwinrc"."Desktops"."Id_1" = "8dc094e3-e9cb-45f9-ae6b-673e2777cd14";
      # "kwinrc"."Desktops"."Id_2" = "e68060eb-c191-4960-92fb-1cd7f8f002fe";
      # "kwinrc"."Desktops"."Id_3" = "21f1ee85-7104-4e9f-a363-47f42cb86406";
      # "kwinrc"."Desktops"."Id_4" = "59788bf8-250e-480d-820a-d1af3f6a5014";
      # "kwinrc"."Desktops"."Id_5" = "4650642b-ce9a-4e2e-93e7-291ce75a4436";
      "kwinrc"."Effect-blurplus"."BlurDecorations" = true;
      "kwinrc"."Effect-blurplus"."BlurMatching" = false;
      "kwinrc"."Effect-blurplus"."BlurMenus" = false;
      "kwinrc"."Effect-blurplus"."BlurNonMatching" = true;
      "kwinrc"."Effect-blurplus"."BlurStrength" = 10;
      "kwinrc"."Effect-blurplus"."PaintAsTranslucent" = true;
      "kwinrc"."Effect-Round-Corners"."ActiveOutlinePalette" = 2;
      "kwinrc"."Effect-Round-Corners"."ActiveOutlineUseCustom" = false;
      "kwinrc"."Effect-Round-Corners"."ActiveOutlineUsePalette" = true;
      "kwinrc"."Effect-Round-Corners"."ActiveShadowPalette" = 12;
      "kwinrc"."Effect-Round-Corners"."DisableOutlineTile" = false;
      "kwinrc"."Effect-Round-Corners"."DisableRoundTile" = false;
      "kwinrc"."Effect-Round-Corners"."InactiveOutlineThickness" = 0;
      "kwinrc"."Effect-Round-Corners"."InactiveShadowSize" = 5;
      "kwinrc"."Effect-Round-Corners"."OutlineColor" = "255,0,0";
      "kwinrc"."Effect-Round-Corners"."OutlineThickness" = 0;
      "kwinrc"."Effect-Round-Corners"."ShadowColor" = "255,108,130";
      "kwinrc"."Effect-Round-Corners"."ShadowSize" = 15;
      "kwinrc"."Effect-Round-Corners"."Size" = 5;
      "kwinrc"."MaximizeTile"."DisableOutlineTile" = false;
      "kwinrc"."MaximizeTile"."DisableRoundTile" = false;
      "kwinrc"."Plugins"."contrastEnabled" = false;
      "kwinrc"."Plugins"."forceblurEnabled" = true;
      "kwinrc"."Plugins"."krohnkiteEnabled" = true;
      "kwinrc"."Plugins"."screenedgeEnabled" = false;
      "kwinrc"."PrimaryOutline"."ActiveOutlineUseCustom" = false;
      "kwinrc"."PrimaryOutline"."ActiveOutlineUsePalette" = true;
      "kwinrc"."PrimaryOutline"."InactiveOutlineThickness" = 0;
      "kwinrc"."PrimaryOutline"."OutlineThickness" = 0;
      "kwinrc"."Script-krohnkite"."enableQuarterLayout" = true;
      "kwinrc"."Script-krohnkite"."enableBTreeLayout" = true;
      "kwinrc"."Script-krohnkite"."enableSpreadLayout" = true;
      "kwinrc"."Script-krohnkite"."enableStairLayout" = true;
      "kwinrc"."Script-krohnkite"."monocleMaximize" = false;
      "kwinrc"."Script-krohnkite"."screenGapBottom" = 15;
      "kwinrc"."Script-krohnkite"."screenGapLeft" = 15;
      "kwinrc"."Script-krohnkite"."screenGapRight" = 15;
      "kwinrc"."Script-krohnkite"."screenGapTop" = 20;
      "kwinrc"."Script-krohnkite"."tileLayoutGap" = 15;
      "kwinrc"."SecondOutline"."ActiveSecondOutlineUseCustom" = false;
      "kwinrc"."SecondOutline"."ActiveSecondOutlineUsePalette" = true;
      "kwinrc"."SecondOutline"."SecondOutlineThickness" = 2;
      "kwinrc"."Shadow"."ActiveShadowPalette" = 12;
      "kwinrc"."Tiling"."padding" = 4;
      "kwinrc"."Tiling/0021ba8e-4ea9-5733-a032-117be6fa070f"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/1a1e8176-6def-531e-8a41-96a58c65d5ad"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/42822bf1-f385-54d2-8c54-66bf719c76d2"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/7b7f8582-1a37-5180-b75f-b3af4bf54d76"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/85cecc54-2c91-5e48-860f-35979f26346d"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/9a56fa2e-b358-55d4-b85b-d3f139b48738"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/a05b08b5-78b2-5a9c-ba49-492351917573"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/bdfecd4f-9ebd-5aef-83f9-e3c6a45616bd"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/d67b1509-69a3-585f-adcc-1048a4265c4f"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Tiling/ed39b2a4-1e89-575d-8f80-864a4608c8f4"."tiles" =
        "{\"layoutDirection\":\"horizontal\",\"tiles\":[{\"width\":0.25},{\"width\":0.5},{\"width\":0.25}]}";
      "kwinrc"."Windows"."AutoRaise" = true;
      "kwinrc"."Windows"."AutoRaiseInterval" = 500;
      "kwinrc"."Windows"."ClickRaise" = false;
      "kwinrc"."Windows"."DelayFocusInterval" = 150;
      "kwinrc"."Windows"."FocusPolicy" = "FocusFollowsMouse";
      "kwinrc"."Xwayland"."Scale" = 1.25;
      "kwinrc"."Round-Corners"."InactiveShadowColor" = "170,255,255";
      "kwinrc"."Round-Corners"."ShadowColor" = "255,103,138";
      "kwinrc"."Round-Corners"."ShadowSize" = 10;
      "kwinrulesrc"."1bc28c62-c079-484e-9596-5e9f5c03e2f1"."Description" = "blur";
      "kwinrulesrc"."1bc28c62-c079-484e-9596-5e9f5c03e2f1"."opacityactive" = 90;
      "kwinrulesrc"."1bc28c62-c079-484e-9596-5e9f5c03e2f1"."opacityactiverule" = 2;
      "kwinrulesrc"."1bc28c62-c079-484e-9596-5e9f5c03e2f1"."opacityinactive" = 95;
      "kwinrulesrc"."1bc28c62-c079-484e-9596-5e9f5c03e2f1"."opacityinactiverule" = 2;
      "kwinrulesrc"."1bc28c62-c079-484e-9596-5e9f5c03e2f1"."wmclasscomplete" = true;
      "kwinrulesrc"."1bc28c62-c079-484e-9596-5e9f5c03e2f1"."wmclassmatch" = 3;
      "kwinrulesrc"."General"."count" = 2;
      "kwinrulesrc"."General"."rules" =
        "c617c6fa-9447-476c-8a48-0e125d6dee03,1bc28c62-c079-484e-9596-5e9f5c03e2f1";
      "kwinrulesrc"."c617c6fa-9447-476c-8a48-0e125d6dee03"."Description" = "no-blur";
      "kwinrulesrc"."c617c6fa-9447-476c-8a48-0e125d6dee03"."opacityactiverule" = 2;
      "kwinrulesrc"."c617c6fa-9447-476c-8a48-0e125d6dee03"."opacityinactiverule" = 2;
      "kwinrulesrc"."c617c6fa-9447-476c-8a48-0e125d6dee03"."wmclass" = no-blur;
      "kwinrulesrc"."c617c6fa-9447-476c-8a48-0e125d6dee03"."wmclassmatch" = 3;
      "plasma-localerc"."Formats"."LANG" = "en_US.UTF-8";
      "plasma-localerc"."Translations"."LANGUAGE" = "en_US";
      "plasmanotifyrc"."Applications/brave-browser"."Seen" = true;
      # "plasmanotifyrc"."Applications/firefox"."Seen" = true;
      "plasmanotifyrc"."Applications/thunderbird"."Seen" = true;
      "plasmanotifyrc"."Applications/vesktop"."Seen" = true;
      "plasmarc"."Wallpapers"."usersWallpapers" =
        "/home/karim/Pictures/Wallpapers/catppuccin-wallpaper-1.jpg,/home/karim/Pictures/Wallpapers/catppuccin-wallpaper-2.jpg,/home/karim/Pictures/Wallpapers/catppuccin-wallpaper-3.png,/home/karim/Pictures/Wallpapers/catppuccin-wallpaper-4.jpg,/home/karim/Pictures/Wallpapers/full-moon.jpg,/home/karim/Pictures/Wallpapers/dark-magical-forest.jpg,/nix/store/421axr0r79b7nnklw27s8wsqpj024kaa-plasma-workspace-wallpapers-6.0.5/share/wallpapers/DarkestHour/contents/images/2560x1440.jpg,/home/karim/Pictures/Wallpapers/spaceig.png,/home/karim/Pictures/Wallpapers/sergey-savvin-forest-night.jpg,/home/karim/Pictures/Wallpapers/nord_valley.png,/home/karim/Pictures/Wallpapers/comet.jpg";
    };
  };
}
