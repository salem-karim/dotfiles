{ pkgs, ... }:

{
  # List services that you want to enable:

  hardware.logitech.wireless = {
    enable = true;
    enableGraphical = true;
  };

  # Enable Blutetooth, ThunderBolt & Framewrok Utils
  hardware.bluetooth.enable = true;
  services.hardware.bolt.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };

  # Enable sound.
  security.rtkit.enable = true;
  services.pulseaudio.enable = false;
  services.pipewire = {
    enable = true;
    audio.enable = true;
    pulse.enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    jack.enable = true;
  };

  # Enable some Services
  services = {
    openssh.enable = true;
    gvfs.enable = true;
    flatpak.enable = true;
    postgresql.enable = true;
    fwupd.enable = true;
  };

  systemd.services.flatpak-repo = {
    wantedBy = [ "multi-user.target" ];
    path = [ pkgs.flatpak ];
    script = ''
      flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    '';
  };
}
