{
  config,
  lib,
  pkgs,
  ...
}:

{
  networking.hostName = "nixos"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # Enable network
  networking.networkmanager = {
    enable = true;
    plugins = with pkgs; [
      networkmanager-openvpn
    ];
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Open ports in the firewall.
  networking.firewall = {
    enable = true;
    checkReversePath = "loose";
    allowedTCPPorts = [
      80 # HTTP
      443 # HTTPS
      8080 # Web Server
      5432 # PostgreSQL
      22 # SSH
      # BambuLab Printer
      1990
      2021
      8883
      990
      322
      6000
      123
    ];

    allowedUDPPorts = [
      123
      1900
      2021
      1195
    ];

    allowedTCPPortRanges = [
      {
        from = 50000;
        to = 50100;
      }
    ];

    trustedInterfaces = [ "tun0" ];

  };

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
}
