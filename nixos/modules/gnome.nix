{ pkgs, pkgs-stable, ... }:

{
  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
  };

  environment.gnome.excludePackages = (
    with pkgs;
    [
      epiphany
      gnome-terminal
      gnome-console
    ]
  );

  environment.systemPackages =
    (with pkgs; [
      gnomeExtensions.appindicator
      gnome-tweaks
    ])
    ++ (with pkgs-stable; [
      gnome-extension-manager
    ]);

  environment.variables = {
    XDG_SESSION_DESKTOP = "GNOME";
    XDG_CURRENT_DESKTOP = "GNOME";
    XDG_SESSION_TYPE = "wayland";
    GDK_BACKEND = "wayland";
  };

  services.udev.packages = with pkgs; [ gnome-settings-daemon ];
  programs.dconf.enable = true;
}
