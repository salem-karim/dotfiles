{
  config,
  lib,
  pkgs,
  pkgs-stable,
  inputs,
  ...
}:

{
  # Enable Plasma 6
  services.desktopManager.plasma6.enable = true;
  services.displayManager.defaultSession = "plasma";
  security.pam.services.sddm.kwallet.enable = true;
  services.displayManager.sddm = {
    enable = true;
    wayland.enable = true;
    theme = "catppuccin-mocha";
    settings = {
      Theme.CursorTheme = "Catppuccin-Mocha-Dark";
      General.PasswordShowLastLetter = 1000;
    };
  };

  services.xserver.enable = true;

  environment.variables = {
    XDG_CURRENT_DESKTOP = "plasma";
  };

  environment.systemPackages = (
    with pkgs;
    [
      kdePackages.krohnkite
      kdePackages.korganizer
      kdePackages.plasma-thunderbolt
      kdePackages.kweather
      kdePackages.kdepim-addons
      kdePackages.kdepim-runtime
      kdePackages.akonadi
      kdePackages.akonadi-calendar
      kdePackages.akonadi-calendar-tools
      kdePackages.akonadi-contacts
      kdePackages.kaccounts-integration
      kdePackages.kaccounts-providers
      kdePackages.signond
      kdePackages.kio-gdrive
      kdePackages.filelight
      kdePackages.kcalc
      kdePackages.kwalletmanager
      kdePackages.kwallet-pam
      kdePackages.kwallet
      # Theming
      inputs.kwin-effects-forceblur.packages.${pkgs.system}.default
      (pkgs.catppuccin-kde.override {
        flavour = [ "mocha" ];
        accents = [ "teal" ];
        winDecStyles = [ "classic" ];
      })
      (pkgs.catppuccin-sddm.override {
        flavor = "mocha";
        font = "Noto Sans";
        fontSize = "10";
        background = "/etc/nixos/Wallpapers/sergey-savvin-forest-night.jpg";
        loginBackground = true;
      })
    ]
  );
  # ++ (with pkgs-stable; [
  # ]);

  environment.plasma6.excludePackages = with pkgs.kdePackages; [
    # konsole
    kate
    dolphin
    xwaylandvideobridge
  ];

  programs.dconf.enable = true;
}
