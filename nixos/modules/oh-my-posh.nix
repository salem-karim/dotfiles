{ ... }:

{
  programs.oh-my-posh = {
    enable = true;
    enableZshIntegration = true;
    enableBashIntegration = true;
    settings = {
      console_title_template = "{{ .Shell }} in {{ .Folder }}";
      final_space = true;
      version = 2;
      transient_prompt = {
        foreground_templates = [
          "{{ if gt .Code 0 }}#ff0000{{ end }}"
          "{{ if eq .Code 0 }}#5fd700{{ end }}"
        ];
        template = "❯";
      };
      blocks = [
        {
          alignment = "left";
          type = "prompt";
          newline = true;
          segments = [
            {
              background = "#b4befe";
              foreground = "#45475a";
              style = "powerline";
              leading_powerline_symbol = "";
              powerline_symbol = "";
              template = "{{.Icon}} ";
              type = "os";
            }
            {
              background = "#a6e3a1";
              foreground = "#45475a";
              style = "powerline";
              powerline_symbol = "";
              template = "  {{ .Path }} ";
              type = "path";
              properties = {
                home_icon = "~";
                style = "full";
              };
            }
            {
              background = "#89b4fa";
              background_templates = [
                "{{ if or (.Working.Changed) (.Staging.Changed) }}#f9e2af{{ end }}"
                "{{ if and (gt .Ahead 0) (gt .Behind 0) }}#f26d50{{ end }}"
                "{{ if gt .Ahead 0 }}#89d1dc{{ end }}"
                "{{ if gt .Behind 0 }}#f2cdcd{{ end }}"
              ];
              foreground = "#45475a";
              style = "powerline";
              powerline_symbol = "";
              template = " {{ .UpstreamIcon }} {{ .HEAD }}{{if .BranchStatus }} {{ .BranchStatus }}{{ end }}{{ if .Working.Changed }}  {{ .Working.String }}{{ end }}{{ if and (.Working.Changed) (.Staging.Changed) }} |{{ end }}{{ if .Staging.Changed }}  {{ .Staging.String }}{{ end }}{{ if gt .StashCount 0 }}  {{ .StashCount }}{{ end }} ";
              type = "git";
              properties = {
                branch_icon = " ";
                fetch_stash_count = true;
                fetch_status = true;
                github_icon = "";
                gitlab_icon = "";
                fetch_upstream_icon = true;
              };
            }
          ];
        }
        {
          alignment = "right";
          type = "prompt";
          segments = [
            {
              background = "#fab387";
              foreground = "#45475a";
              style = "powerline";
              invert_powerline = false;
              powerline_symbol = "";
              leading_powerline_symbol = "";
              template = " {{ .FormattedMs }}  ";
              type = "executiontime";
              properties = {
                threshold = 500;
                style = "round";
                always_enabled = true;
              };
            }
          ];
        }
        {
          alignment = "left";
          newline = true;
          type = "prompt";
          segments = [
            {
              foreground_templates = [
                "{{ if gt .Code 0 }}#ff0000{{ end }}"
                "{{ if eq .Code 0 }}#5fd700{{ end }}"
              ];
              style = "plain";
              template = "❯";
              type = "text";
            }
          ];
        }
      ];
    };
  };
}
