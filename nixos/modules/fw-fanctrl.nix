{ config, pkgs, ... }:

{
  # Enable and configure fw-fanctrl
  programs.fw-fanctrl.enable = true;

  programs.fw-fanctrl.config = {
    defaultStrategy = "lazy";
    strategyOnDischarging = "lazy";
    strategies = {
      laziest = {
        fanSpeedUpdateFrequency = 5;
        movingAverageInterval = 40;
        # name = "laziest";
        speedCurve = [
          {
            temp = 0;
            speed = 0;
          }
          {
            temp = 45;
            speed = 0;
          }
          {
            temp = 65;
            speed = 25;
          }
          {
            temp = 70;
            speed = 35;
          }
          {
            temp = 75;
            speed = 50;
          }
          {
            temp = 80;
            speed = 75;
          }
          {
            temp = 85;
            speed = 100;
          }
        ];
      };
      lazy = {
        fanSpeedUpdateFrequency = 5;
        movingAverageInterval = 30;
        # name = "lazy";
        speedCurve = [
          {
            temp = 0;
            speed = 15;
          }
          {
            temp = 50;
            speed = 15;
          }
          {
            temp = 65;
            speed = 25;
          }
          {
            temp = 70;
            speed = 35;
          }
          {
            temp = 75;
            speed = 50;
          }
          {
            temp = 80;
            speed = 75;
          }
          {
            temp = 85;
            speed = 100;
          }
        ];
      };
      medium = {
        fanSpeedUpdateFrequency = 5;
        movingAverageInterval = 30;
        # name = "medium";
        speedCurve = [
          {
            temp = 0;
            speed = 15;
          }
          {
            temp = 40;
            speed = 15;
          }
          {
            temp = 60;
            speed = 30;
          }
          {
            temp = 70;
            speed = 40;
          }
          {
            temp = 75;
            speed = 60;
          }
          {
            temp = 80;
            speed = 80;
          }
          {
            temp = 85;
            speed = 100;
          }
        ];
      };
      agile = {
        fanSpeedUpdateFrequency = 3;
        movingAverageInterval = 15;
        # name = "agile";
        speedCurve = [
          {
            temp = 0;
            speed = 15;
          }
          {
            temp = 40;
            speed = 15;
          }
          {
            temp = 60;
            speed = 30;
          }
          {
            temp = 70;
            speed = 40;
          }
          {
            temp = 75;
            speed = 60;
          }
          {
            temp = 80;
            speed = 80;
          }
          {
            temp = 85;
            speed = 100;
          }
        ];
      };
      very-agile = {
        fanSpeedUpdateFrequency = 2;
        movingAverageInterval = 5;
        # name = "very-agile";
        speedCurve = [
          {
            temp = 0;
            speed = 15;
          }
          {
            temp = 40;
            speed = 15;
          }
          {
            temp = 60;
            speed = 30;
          }
          {
            temp = 70;
            speed = 40;
          }
          {
            temp = 75;
            speed = 80;
          }
          {
            temp = 85;
            speed = 100;
          }
        ];
      };
      deaf = {
        fanSpeedUpdateFrequency = 2;
        movingAverageInterval = 5;
        # name = "deaf";
        speedCurve = [
          {
            temp = 0;
            speed = 20;
          }
          {
            temp = 40;
            speed = 30;
          }
          {
            temp = 50;
            speed = 50;
          }
          {
            temp = 60;
            speed = 100;
          }
        ];
      };
      aeolus = {
        fanSpeedUpdateFrequency = 2;
        movingAverageInterval = 5;
        # name = "aeolus";
        speedCurve = [
          {
            temp = 0;
            speed = 20;
          }
          {
            temp = 40;
            speed = 50;
          }
          {
            temp = 65;
            speed = 100;
          }
        ];
      };
    };
  };
}
