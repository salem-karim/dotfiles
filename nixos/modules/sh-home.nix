{ pkgs, ... }:
let
  Aliases = {
    ls = "ls --color";
    l = "eza --icons --color=always -l --git --no-filesize --no-user --no-time --no-permissions";
    ll = "eza --icons --color=always -l --git";
    lll = "eza --icons --color=always -la --git";
    open = "xdg-open";
    tl = "eza -T --icons -L 5";
    lg = "lazygit";
    ldo = "lazydocker";
    vim = "nvim";
    vi = "nvim";
    ta = "tmux a";
    logres = "sudo systemctl restart logiops";
    flakeup = "nix flake update --flake /home/karim/dotfiles/nixos";
    nixdev = "nix develop --command zsh";
  };
in
{
  programs.zsh = {
    enable = true;
    shellAliases = Aliases;
    history = {
      size = 5000;
      path = "$HOME/.cache/zsh/.zsh_history";
      save = 5000;
      ignoreDups = true;
      ignoreSpace = true;
      expireDuplicatesFirst = true;
      share = true;
    };
    initExtra = ''
      source "${pkgs.zinit}/share/zinit/zinit.zsh"

      zinit light zsh-users/zsh-syntax-highlighting
      zinit light zsh-users/zsh-completions
      zinit light zsh-users/zsh-autosuggestions
      zinit light Aloxaf/fzf-tab
      zinit light softmoth/zsh-vim-mode

      zinit snippet OMZP::sudo
      zinit snippet OMZP::command-not-found
      zinit cdreplay -q

      zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
      zstyle ':completion:*' menu no

      zstyle ':fzf-tab:*' switch-group '<' '>'
      zstyle ':fzf-tab:*' fzf-flags --height=50% --info=inline --border --preview-window='right:50%:wrap'
      zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza --color=always --icons -la --git --no-time $realpath'
      zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'eza --color=always --icons -la --git --no-time $realpath'
      zstyle ':fzf-tab:complete:*' fzf-preview '{
        item=$realpath
        if [[ -d "$item" ]]; then
          eza --color=always --icons -la --git --no-time "$item"
        elif [[ -f "$item" ]]; then
          bat --color=always --style=numbers --line-range=:200 "$item"
        else
          echo "Not a file or directory"
        fi
      }'
      bindkey '^J' down-line-or-search
      bindkey '^K' atuin-up-search-viins

      # Define a function to run `yy`
      yazi_widget() {
        zle -I          # Clear pending input
        BUFFER="yy"     # Set the command to 'yy'
        zle accept-line # Simulate pressing Enter
      }

      # Register the function as a ZLE widget and bind it to Ctrl-Y
      zle -N yazi_widget
      bindkey '^Y' yazi_widget
      nvim_widget() {
        zle -I          # Clear pending input
        BUFFER="nvim"   # Set the command to 'nvim'
        zle accept-line # Simulate pressing Enter
      }
      zle -N nvim_widget
      bindkey '^A' nvim_widget 
      if [ "$TERM" = "xterm-kitty" ] && command -v tmux &> /dev/null && [ -z "$TMUX" ] && [ -z "$SSH_CONNECTION" ]; then
        tmux attach-session -t common || tmux new-session -s common
      fi

      # Enable 1Password support
      source /home/karim/.config/op/plugins.sh
      eval "$(op completion zsh)"; compdef _op op

      flakeinit () {
        # Define the base URL for the template

        # Check if the argument is provided
        if [ -z "$1" ]; then
          echo "Please provide a template (e.g., python, rust)"
          return 1
        fi

        # Concatenate the base URL with the argument

        # Run nix flake init with the selected template
        nix flake init -t "https://flakehub.com/f/the-nix-way/dev-templates/*#$1"
      }
    '';
  };

  programs.bash = {
    enable = true;
    shellAliases = Aliases;
    initExtra = ''
      export NIXPKGS_ALLOW_UNFREE=1
      export EDITOR="nvim"
      # Ble.sh config
      source ${pkgs.blesh}/share/blesh/ble.sh
      ble-face auto_complete=fg=245
      ble-face command_directory=fg=26
      ble-face filename_directory=fg=26
      ble-face region_insert=fg=black,bg=252

      function nvims() {
        items=( "nvim" "LazyVim" )
        config=$(printf "%s\n" "''${items[@]}" | fzf --prompt=" Neovim Config  " --height=~50% --layout=reverse --border --exit-0)
        if [[ -z $config ]]; then
          echo "Nothing selected"
          return 0
        elif [[ $config == "nvim" ]]; then
          config=""
        fi
        NVIM_APPNAME=$config nvim $@
      }
      bind -x '"\C-a": nvims'
      bind -x '"\C-f": fzf'
    '';
  };

  programs.zoxide = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
    options = [ "--cmd cd" ];
  };

  programs.fzf = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
  };

  programs.atuin = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
    settings = {
      enter_accept = true;
      records = true;
      auto_sync = true;
      theme = {
        name = "marine";
      };
    };
  };

  programs.eza = {
    enable = true;
    icons = "auto";
    git = true;
    extraOptions = [ "--color=always" ];
    enableZshIntegration = true;
    enableBashIntegration = true;
  };
}
