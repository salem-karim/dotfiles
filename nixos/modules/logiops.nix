{ config, pkgs, ... }:

{
  # Create systemd service
  # https://github.com/PixlOne/logiops/blob/5547f52cadd2322261b9fbdf445e954b49dfbe21/src/logid/logid.service.in
  systemd.services.logiops = {
    description = "Logitech Configuration Daemon";
    wantedBy = [ "graphical.target" ];
    startLimitIntervalSec = 0;
    after = [ "multi-user.target" ];
    wants = [ "multi-user.target" ];
    serviceConfig = {
      # ExecStart = "${pkgs.logiops_0_2_3}/bin/logid -v";
      ExecStart = "${pkgs.logiops}/bin/logid";
      Restart = "always";
      User = "root";
      RuntimeDirectory = "logiops";

      CapabilityBoundingSet = [ "CAP_SYS_NICE" ];
      DeviceAllow = [
        "/dev/uinput rw"
        "char-hidraw rw"
      ];
      ProtectClock = true;
      PrivateNetwork = true;
      ProtectHome = true;
      ProtectHostname = true;
      PrivateUsers = true;
      PrivateMounts = true;
      PrivateTmp = true;
      RestrictNamespaces = true;
      ProtectKernelLogs = true;
      ProtectKernelModules = true;
      ProtectKernelTunables = true;
      ProtectControlGroups = true;
      MemoryDenyWriteExecute = true;
      RestrictRealtime = true;
      LockPersonality = true;
      ProtectProc = "invisible";
      SystemCallFilter = [
        "nice"
        "@system-service"
        "~@privileged"
      ];
      RestrictAddressFamilies = [
        "AF_NETLINK"
        "AF_UNIX"
      ];
      RestrictSUIDSGID = true;
      NoNewPrivileges = true;
      ProtectSystem = "strict";
      ProcSubset = "pid";
      UMask = "0077";
    };
  };

  # Configuration for logiops
  environment.etc."logid.cfg".text = ''
        devices: (
          {
            name: "MX Master 3S";
            timeout: 5000;
            smartshift: {
              on: true;
              threshold: 20;
            };
        
            hiresscroll: {
              hires: false;
              invert: false;
              target: false;
            };
        
            dpi: 4000; // max=4000
        
            buttons: (
              // Forward button
              {
                cid: 0x56;
                action: {
                  type: "Gestures";
                  gestures: (
                    {
                      direction: "None";
                      mode: "OnRelease";
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTCTRL", "KEY_C" ];
                      }
                    },
    /* 
                    {
                      direction: "Up";
                      mode: "OnRelease";
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA", "KEY_LEFTSHIFT", "KEY_LEFTCTRL", "KEY_W" ];   // Increase Master-Stack
                      }
                    },
        
                    {
                      direction: "Down";
                      mode: "OnRelease";
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA", "KEY_LEFTSHIFT", "KEY_LEFTCTRL", "KEY_S" ];   // Decrease Master-Stack
                      }
                    },
    */ 
                    {
                      direction: "Right";
                      # mode: "OnRelease";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTCTRL", "KEY_LEFTMETA", "KEY_LEFT" ]; // goes one Workspace to the left
                      }
                    },
        
                    {
                      direction: "Left";
                      # mode: "OnRelease";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTCTRL", "KEY_LEFTMETA", "KEY_RIGHT" ]; // goes one Workspace to the right
                      }
                    }
                  );
                };
              },
        
              // Back button
              {
                cid: 0x53;
                action: {
                  type: "Gestures";
                  gestures: (
                    {
                      direction: "None";
                      mode: "OnRelease";
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTCTRL", "KEY_V" ];
                      }
                    },
        
                    {
                      direction: "Up";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA", "KEY_LEFTCTRL", "KEY_K" ]; //moves WM Windows one upwards 
                      }
                    },
        
                    {
                      direction: "Down";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA", "KEY_LEFTCTRL", "KEY_J" ]; //moves WM Windows one downwards 
                      }
                    },
        
                    {
                      direction: "Right";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA", "KEY_LEFTCTRL", "KEY_L" ]; //moves WM Windows one to the right 
                      }
                    },
        
                    {
                      direction: "Left";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_RIGHTMETA", "KEY_LEFTCTRL", "KEY_H" ]; //moves WM Windows one to the left 
                      }
                    }
                  );
                };
              },
        
              // Gesture button (hold and move)
              {
                cid: 0xc3;
                action: {
                  type: "Gestures";
                  gestures: (
                    {
                      direction: "None";
                      mode: "OnRelease";
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA" ]; // open activities
                      }
                    },
        
                    {
                      direction: "Right";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA", "KEY_LEFTALT", "KEY_L" ]; // move window to right workspace
                      }
                    },
        
                    {
                      direction: "Left";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_RIGHTMETA", "KEY_LEFTALT", "KEY_H" ]; // move window to left workspace
                      }
                    },
        
                    {
                      direction: "Down";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA", "KEY_A" ]; // overview
                      }
                    },
        
                    {
                      direction: "Up";
                      mode: "OnRelease";
                      action: {
                        type: "Keypress";
                        keys: [ "KEY_LEFTMETA", "KEY_X" ]; // Toggle between Floating and Tiling 
                      }
                    }
                  );
                };
              },
        
              // Top button
              {
                cid: 0xc4;
                action: {
                  type: "Gestures";
                  gestures: (
                    {
                      direction: "None";
                      mode: "OnRelease";
                      action: {
                        type: "ToggleSmartShift";
                      }
                    },
        
                    {
                      direction: "Up";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "ChangeDPI";
                        inc: 100,
                      }
                    },
        
                    {
                      direction: "Down";
                      mode: "OnThreshold";
                      threshold: 450;
                      action: {
                        type: "ChangeDPI";
                        inc: -100,
                      }
                    }
                  );
                };
              }
            );
          }
        );
  '';
}
