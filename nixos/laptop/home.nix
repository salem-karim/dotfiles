{
  config,
  pkgs,
  # pkgs-stable,
  laptopdeskenv,
  ...
}:
let
  commonAliases = import ../modules/sh-home.nix { inherit config pkgs; };
  laptopAliases = commonAliases.programs.zsh.shellAliases // {
    sysup = "nh os switch -H laptop";
    sysupgrade = "nh os switch -H laptop -u";
    homeup = "nh home switch -c laptop";
  };
  commonShortcuts = import ../modules/kde-home.nix { inherit config pkgs; };
in
{

  programs.zsh.shellAliases = laptopAliases;
  programs.bash.shellAliases = laptopAliases;

  programs.plasma = (
    if laptopdeskenv == "plasma" then
      {
        input.touchpads = [
          {
            name = "PIXA3854:00 093A:0274 Touchpad";
            enable = true;
            disableWhileTyping = true;
            leftHanded = false;
            middleButtonEmulation = true;
            pointerSpeed = 0.2;
            naturalScroll = true;
            tapToClick = true;
            tapAndDrag = false;
            tapDragLock = false;
            twoFingerTap = "rightClick";
            scrollMethod = "twoFingers";
            rightClickMethod = "twoFingers";
            vendorId = "093a";
            productId = "0274";
          }
        ];
        powerdevil = {
          AC = {
            autoSuspend.action = "sleep";
            autoSuspend.idleTimeout = 900;
            powerButtonAction = "shutDown";
            whenLaptopLidClosed = "sleep";
            whenSleepingEnter = "standbyThenHibernate";
            dimDisplay.enable = true;
            dimDisplay.idleTimeout = 300;
            turnOffDisplay.idleTimeout = 600;
            turnOffDisplay.idleTimeoutWhenLocked = 60;
          };
          battery = {
            autoSuspend.action = "sleep";
            autoSuspend.idleTimeout = 600;
            powerButtonAction = "showLogoutScreen";
            whenLaptopLidClosed = "sleep";
            whenSleepingEnter = "standbyThenHibernate";
            dimDisplay.enable = true;
            dimDisplay.idleTimeout = 120;
            turnOffDisplay.idleTimeout = 300;
            turnOffDisplay.idleTimeoutWhenLocked = 60;
          };
          lowBattery = {
            autoSuspend.action = "sleep";
            autoSuspend.idleTimeout = 900;
            powerButtonAction = "shutDown";
            whenLaptopLidClosed = "sleep";
            whenSleepingEnter = "standbyThenHibernate";
            dimDisplay.enable = true;
            dimDisplay.idleTimeout = 60;
            turnOffDisplay.idleTimeout = 120;
            turnOffDisplay.idleTimeoutWhenLocked = 60;
          };
        };
        shortcuts = commonShortcuts.programs.plasma.shortcuts // {
          "services/org.kde.dolphin.desktop"."_launch" = "Meta+F";
          "services/org.kde.krunner.desktop"."_launch" = [
            "Meta+Space"
            "Alt+F2"
            "Search"
          ];
        };

      }
    else
      { }
  );

  home.sessionVariables = {
    EDITOR = "nvim";
    # XDG_CURRENT_DESKTOP = "plasma";
  };

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "24.11"; # Please read the comment before changing.
}
