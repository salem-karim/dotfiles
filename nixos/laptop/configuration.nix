{
  lib,
  pkgs,
  pkgs-stable,
  # laptopdeskenv,
  ...
}:
{

  # List packages installed systemwide
  environment.systemPackages =
    (with pkgs; [
      sbctl
      tpm2-tss
    ])
    ++ (with pkgs-stable; [
      fprintd
      fw-ectool
    ]);

  boot.initrd.systemd.enable = true;

  boot.kernelPackages = pkgs.linuxPackages_zen;

  security.pam.services.login.fprintAuth = false;
  security.pam.services.fprint = {
    text = ''
      auth            sufficient      pam_fprintd.so
      auth            sufficient      pam_unix.so try_first_pass likeauth nullok
      auth            include         login
      account         include         login
      password        include         login
      session         include         login
    '';
  };

  security.pam.services.polkit-1 = {
    text = ''
      # Account management.
      account required pam_unix.so # unix (order 10900)

      # Authentication management.
      auth sufficient pam_unix.so likeauth try_first_pass # unix (order 11500)
      auth sufficient  ${pkgs.fprintd}/lib/security/pam_fprintd.so # fprintd (order 11300)
      auth required pam_deny.so # deny (order 12300)

      # Password management.
      password sufficient pam_unix.so nullok yescrypt # unix (order 10200)

      # Session management.
      session required pam_env.so conffile=/etc/pam/environment readenv=0 # env (order 10100)
      session required pam_unix.so # unix (order 10200)
    '';
  };

  # Enable touchpad, fingerprint and framework updater
  services.libinput.enable = true;
  services.fprintd.enable = true;

  boot.loader.systemd-boot.enable = lib.mkForce false;
  boot.lanzaboote = {
    enable = true;
    pkiBundle = "/var/lib/sbctl";
  };

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;
  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.11"; # Did you read the comment?
}
