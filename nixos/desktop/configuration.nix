{
  inputs,
  lib,
  config,
  pkgs,
  pkgs-stable,
  # desktopdeskenv
  ...
}:
let
  commonConfig = import ../common/configuration.nix {
    inherit
      config
      lib
      inputs
      pkgs
      pkgs-stable
      ;
  };
in
{
  boot.kernelPackages = pkgs.linuxPackages_zen;
  # services.scx.enable = true;

  # List packages installed systemwide
  environment.systemPackages =
    (with pkgs; [
      umu-launcher
      sbctl
      mangohud
      shadps4
      nexusmods-app
      davinci-resolve
      wineWowPackages.staging
      winetricks
      lm_sensors
      # lact
    ])
    ++ (with pkgs-stable; [
      tpm2-tss
      phoronix-test-suite
    ]);

  # systemd.services.lactd = {
  #   description = "AMDGPU Control Daemon";
  #   after = [ "multi-user.target" ];
  #   wantedBy = [ "multi-user.target" ];
  #   serviceConfig = {
  #     ExecStart = "${pkgs.lact}/bin/lact daemon";
  #   };
  #   enable = true;
  # };

  # environment.variables = commonConfig.environment.variables // {
  #   DRI_PRIME = "1";
  # };

  #disable the power-profiles-daemon
  services.power-profiles-daemon.enable = false;
  # programs.coolercontrol.enable = true;

  programs.corectrl = {
    enable = true;
    gpuOverclock = {
      enable = true;
      ppfeaturemask = "0xffffffff";
    };
  };

  boot.kernelParams = [
    "amdgpu.dc=1"
    "amdgpu.aspm=0"
    "usbcore.autosuspend=-1"
    "video=DP-1:2560x1440@170" # Gigabyte M27Q DP
    "video=DP-5:2560x1440@60" # Dell U2518D
  ];

  boot.initrd.systemd.enable = true;

  systemd.sleep.extraConfig = ''
    AllowSuspend=yes
    AllowHibernation=no
    AllowHybridSleep=no
    AllowSuspendThenHibernate=no 
    MemorySleepMode=deep
    SuspendState=mem
  '';

  # Disable Mouse wakeup
  services.udev.extraRules = ''
    ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="046d", ATTRS{idProduct}=="c548", ATTR{power/wakeup}="disabled"
    ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="1532", ATTRS{idProduct}=="0064", ATTR{power/wakeup}="disabled"
  '';

  boot.loader.systemd-boot.enable = lib.mkForce false;

  boot.lanzaboote = {
    enable = true;
    pkiBundle = "/var/lib/sbctl";
  };

  systemd.services.restart-logiops-on-wakeup = {
    description = "Restart logiops on wakeup";
    after = [
      "suspend.target"
      "hibernate.target"
      "hybrid-sleep.target"
      "suspend-then-hibernate.target"
    ];
    wantedBy = [
      "suspend.target"
      "hibernate.target"
      "hybrid-sleep.target"
      "suspend-then-hibernate.target"
    ];
    serviceConfig = {
      Type = "oneshot";
      ExecStart = "${config.systemd.package}/bin/systemctl --no-block try-restart logiops.service";
    };
  };

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;
  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.11"; # Did you read the comment?
}
