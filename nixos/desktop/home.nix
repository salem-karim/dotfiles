{
  config,
  pkgs,
  # pkgs-stable,
  desktopdeskenv,
  ...
}:
let
  commonAliases = import ../modules/sh-home.nix { inherit config pkgs; };
  commonShortcuts = import ../modules/kde-home.nix { inherit config pkgs; };
  dektopAliases = commonAliases.programs.zsh.shellAliases // {
    sysup = "nh os switch -H desktop";
    sysupgrade = "nh os switch -H desktop -u";
    homeup = "nh home switch -c desktop";
    resound = "systemctl --user restart pipewire.service && systemctl --user restart pipewire-pulse.service";
  };
in
{
  programs.zsh.shellAliases = dektopAliases;
  programs.bash.shellAliases = dektopAliases;
  programs.plasma = (
    if desktopdeskenv == "plasma" then
      {
        powerdevil.AC = {
          powerButtonAction = "shutDown";
          autoSuspend.action = "sleep";
          autoSuspend.idleTimeout = 1200;
          whenSleepingEnter = "standby";
          dimDisplay.enable = true;
          dimDisplay.idleTimeout = 600;
          turnOffDisplay.idleTimeout = 900;
          turnOffDisplay.idleTimeoutWhenLocked = 60;
          powerProfile = "performance";
        };
        shortcuts = commonShortcuts.programs.plasma.shortcuts // {
          "services/org.gnome.Nautilus.desktop"."_launch" = "Meta+F";
          "services/org.kde.krunner.desktop"."_launch" = [
            "Meta+Del"
            "Alt+F2"
            "Search"
          ];
        };
      }
    else
      { }
  );

  home.sessionVariables = {
    EDITOR = "nvim";
    # XDG_CURRENT_DESKTOP = "plasma";
  };

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "24.11"; # Please read the comment before changing.
}
