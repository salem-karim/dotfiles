{
  pkgs,
  config,
  pkgs-stable,
  ...
}:
# let
#   lib = pkgs.lib;
#
#   ldlibs = with pkgs; [
#     libGL
#     libGLU
#     gtk3
#     cairo
#     pango
#     atk
#     glib
#     gdk-pixbuf
#     xorg.libX11
#     xorg.libX11.dev
#     xorg.libXi
#     xorg.libXxf86vm
#     xorg.libXtst
#   ];
#
# in
{
  # Importing modules
  imports = [
    ../modules/starship.nix
    ../modules/sh-home.nix
    ../modules/oh-my-posh.nix
  ];
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "karim";
  home.homeDirectory = "/home/karim";
  nixpkgs.config.allowUnfree = true;

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages =
    with pkgs;
    [
      #####################
      # dev kit
      icu
      rustup
      rustlings
      go
      ghc
      zig
      cmake
      scenebuilder
      maven
      godot_4
      mono
      shellify
      # Nix
      nixd
      nixfmt-rfc-style
      # C/C++/C#
      clang-tools
      cppcheck
      cmake-language-server
      cmake-format
      csharpier
      netcoredbg
      dotnet-sdk
      # Python
      yapf
      mypy
      pyright
      ruff
      # WebDev
      nodejs
      nodePackages.live-server
      nodePackages.cspell
      nodePackages.bash-language-server
      # Lua & Vim
      stylua
      luajitPackages.luarocks
      lua-language-server
      # Java
      jdt-language-server
      google-java-format
      checkstyle
      lemminx
      lombok
      # Other
      gdtoolkit_4
      zls
      gopls
      codespell
      sqlfluff
      openldap
      openldap.dev
      pkg-config
      #####################
      via
      vial
      trash-cli
      lazygit
      lazydocker
      vscode
      discord
      vesktop
      obs-studio
      starship
      # cemu
      kde-rounded-corners
      brave
      zapzap
      thunderbird
      zoom-us
      anki
      fd
      orca-slicer
      bambu-studio
    ]
    ++ (with pkgs-stable; [
      blender
      biome
      vim-language-server
      unityhub
      zinit
      tree
      gimp
      clang
      omnisharp-roslyn
    ]);
  # # You can also create simple shell scripts directly inside your
  # # environment:
  # (pkgs.writeShellScriptBin "my-hello" ''
  #   echo "Hello, ${config.home.username}!"
  # '')

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  # # Building this configuration will create a copy of 'dotfiles/screenrc' in
  # # the Nix store. Activating the configuration will then make '~/.screenrc' a
  # # symlink to the Nix store copy.
  # ".screenrc".source = dotfiles/screenrc;

  # # You can also set the file content immediately.
  # ".gradle/gradle.properties".text = ''
  #   org.gradle.console=verbose
  #   org.gradle.daemon.idletimeout=3600000
  # '';
  # located at either
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  # or
  #  ~/.local/state/nix/profiles/profile/etc/profile.d/hm-session-vars.sh
  # or
  #  /etc/profiles/per-user/karim/etc/profile.d/hm-session-vars.sh

  home.file.".config/nvim" = {
    source = config.lib.file.mkOutOfStoreSymlink "/home/karim/dotfiles/config/NvChad";
    recursive = true;
  };

  home.file.".config/tmux" = {
    source = config.lib.file.mkOutOfStoreSymlink "/home/karim/dotfiles/config/tmux";
    recursive = true;
  };

  home.file.".config/Code/User/settings.json" = {
    source = config.lib.file.mkOutOfStoreSymlink "/home/karim/dotfiles/config/settings.json";
    recursive = true;
  };

  home.file.".ideavimrc" = {
    source = config.lib.file.mkOutOfStoreSymlink "/home/karim/dotfiles/config/.ideavimrc";
    recursive = true;
  };

  home.sessionVariables = {
    EDITOR = "nvim";
    LOMBOK_HOME = "${pkgs.lombok}";
    PKG_CONFIG_PATH = "${pkgs.openldap.dev}/lib/pkgconfig";
    NIXPKGS_ALLOW_UNFREE = 1;
    GTK_THEME = "Catppuccin-Mocha-Standard-Teal-Dark";
    # LD_LIBRARY_PATH = "${lib.makeLibraryPath ldlibs}";
  };

  programs.java = {
    enable = true;
    package = (
      pkgs.jdk23.override {
        enableJavaFX = true;
        openjfx_jdk = pkgs.openjfx.override {
          # withWebKit = true;
          featureVersion = "23";
        };
      }
    );
  };

  catppuccin = {
    enable = true;
    flavor = "mocha";
    accent = "teal";
    yazi.enable = true;
    lazygit.enable = true;
    btop.enable = true;
    bat.enable = true;
    kvantum = {
      enable = true;
      apply = true;
    };
  };

  # gtk = {
  #   enable = true;
  #   cursorTheme = {
  #     name = "catppuccin-mocha-dark-cursors";
  #     size = 32;
  #     package = pkgs.catppuccin-cursors.mochaDark;
  #   };
  #   font.name = "Noto Sans";
  #   font.size = 10;
  #   theme.name = "Catppuccin-Mocha-Standard-Teal-Dark";
  #   iconTheme.name = "Papirus-Dark";
  # };

  # Configure Git
  programs.git = {
    enable = true;
    userName = "salem-karim";
    userEmail = "salem.karim@icloud.com";
    aliases = {
      c = "commit -am";
      s = "status";
      a = "add .";
      ll = "log --graph --decorate --oneline --all";
      pl = "pull";
      ps = "push";
    };
    extraConfig = {
      pull.rebase = true;
      init.defaultBranch = "main";
      include.path = "/home/karim/.config/git/op.conf";
      credential = {
        "https://gitlab.com" = {
          helper = "!${pkgs.glab}/bin/glab auth git-credential";
        };
      };
    };
  };

  programs.gh = {
    enable = true;
    settings = {
      editor = "nvim";
      # git_protocol = "https";
      git_protocol = "ssh";
      prompt = "enabled";
      browser = "zen";
      aliases = {
        co = "pr checkout";
        pv = "pr view";
      };
    };
  };

  # Theme Bat
  programs.bat = {
    enable = true;
    config = {
      number = true;
      style = "plain";
      wrap = "auto";
      color = "always";
    };
  };

  programs.btop = {
    enable = true;
    settings = {
      theme_background = false;
      truecolor = true;
      vim_keys = true;
      update_ms = 500;
    };
  };

  programs.yazi = {
    enable = true;
    enableZshIntegration = true;
    enableBashIntegration = true;
    settings = {
      manager = {
        sort_by = "alphabetical";
        sort_dir_first = true;
      };
      image = {
        enable = false;
      };
    };
  };

  # Configure kitty
  programs.kitty = {
    enable = true;
    themeFile = "Catppuccin-Mocha"; # This assumes you have the theme installed and available
    shellIntegration.enableZshIntegration = true;
    shellIntegration.enableBashIntegration = true;
    settings = {
      enable_audio_bell = false;
      wayland_titlebar_color = "system";
      background_opacity = "0.80";
      confirm_os_window_close = "0";
      background_blur = 1;
      cursor_shape = "beam";
      cursor_beam_thickness = "1.5";
      # Enable split Layout
      enabled_layouts = "splits:split_axis=vertical";
      # Font settings
      font_family = "JetBrainsMono";
      bold_font = "JetBrainsMono NF Bold";
      italic_font = "JetBrainsMono NF Italic";
      bold_italic_font = "JetBrainsMono NF Bold Italic";
      cursor_trail = 1;
      cursor_trail_decay = "0.2 0.5";
      cursor_trail_start_threshold = 2;
    };
    keybindings = {
      "ctrl+shift+h" = "send_text all \\x1b[72;6u";
      "ctrl+shift+j" = "send_text all \\x1b[74;6u";
      "ctrl+shift+k" = "send_text all \\x1b[75;6u";
      "ctrl+shift+l" = "send_text all \\x1b[76;6u";
      "shift+enter" = "send_text all \\x1b[13;2u";
      "ctrl+enter" = "send_text all \\x1b[13;5u";
      "ctrl+shift+enter" = "send_text all \\x1b[13;6u";
    };
  };
  programs.wofi = {
    enable = true;

    settings = {
      allow_markup = true;
      width = 600;
      show = "drun";
      prompt = "Apps";
      term = "kitty";
      normal_window = true;
      layer = "top";
      height = "305px";
      orientation = "vertical";
      halign = "fill";
      line_wrap = "off";
      dynamic_lines = false;
      allow_images = true;
      image_size = 24;
      exec_search = false;
      hide_search = false;
      parse_search = false;
      insensitive = true;
      hide_scroll = true;
      no_actions = true;
      sort_order = "default";
      filter_rate = 100;
      key_expand = "Tab";
      key_exit = "Escape";
      key_up = "Ctrl-k";
      key_down = "Ctrl-j";
      key_left = "Ctrl-h";
      key_right = "Ctrl-l";
    };
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
