{
  lib,
  inputs,
  pkgs,
  pkgs-stable,
  ...
}:

{
  imports = [
    # Include the results of the hardware scan.
    ../modules/logiops.nix
    ../modules/services.nix
    ../modules/network.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Enable nix-command and Unfree Software
  nixpkgs.config.allowUnfree = true;
  nix.settings = {
    experimental-features = [
      "nix-command"
      "flakes"
    ];
    auto-optimise-store = true;
  };
  # nix.gc = {
  #   automatic = true;
  #   dates = "weekly";
  #   options = "--delete-older-than 7d";
  # };

  programs.nh = {
    enable = true;
    clean = {
      enable = true;
      dates = "weekly";
      extraArgs = "-K 7d";
    };
    flake = "/home/karim/dotfiles/nixos/";
  };
  # For nixd lsp
  nix.nixPath = [ "nixpkgs=${inputs.nixpkgs}" ];

  # Set your time zone.
  time.timeZone = "Europe/Vienna";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_NUMERIC = "de_AT.UTF-8";
    LC_TIME = "de_AT.UTF-8";
    LC_MONETARY = "de_AT.UTF-8";
    LC_MEASUREMENT = "de_AT.UTF-8";
    LC_PAPER = "de_AT.UTF-8";
    LC_ADDRESS = "de_AT.UTF-8";
    LC_NAME = "de_AT.UTF-8";
    LC_TELEPHONE = "de_AT.UTF-8";
  };

  # Configure keymap in X11
  services.xserver.xkb.options = "caps:escape";
  services.xserver.xkb = {
    layout = "at";
    variant = "nodeadkeys";
  };

  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true; # use xkb.options in tty.
  };

  # Enable QMK
  hardware.keyboard.qmk.enable = true;
  services.udev.packages = [
    pkgs.via
    pkgs.vial
  ];

  # Enable VMs
  virtualisation = {
    vmware = {
      host.enable = true;
      # guest.enable = true;
    };
    docker = {
      enable = true;
      storageDriver = "btrfs";
      rootless = {
        enable = true;
        setSocketVariable = true;
      };
    };
    podman.enable = true;
  };

  # Enable OpenGL & driver support
  hardware.graphics = {
    enable = true;
    enable32Bit = true;
    extraPackages = with pkgs; [
      rocmPackages.clr.icd
      # rusticl
    ];
  };
  services.xserver.videoDrivers = [ "amdgpu" ];

  # Set Default Shell
  users.defaultUserShell = pkgs.zsh;
  programs.zsh.enable = true;

  # Enable Steam and set some settings
  programs.steam = {
    enable = true;
    gamescopeSession.enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
    localNetworkGameTransfers.openFirewall = true; # Open ports in the firewall for Steam Local Network Game Transfers
  };
  programs.gamemode.enable = true;

  # Enable 1Password and include polkit
  programs._1password = {
    enable = true;
    package = pkgs._1password-cli;
  };

  programs._1password-gui = {
    enable = true;
    polkitPolicyOwners = [ "karim" ];
    package = pkgs._1password-gui;
  };

  # programs.firefox = {
  #   enable = true;
  #   package = pkgs.firefox-bin;
  #   # preferences = {
  #   #   "widget.use-xdg-desktop-portal.file-picker" = 1;
  #   # };
  # };

  environment.etc = {
    "1password/custom_allowed_browsers" = {
      text = ''
        vivaldi-bin
        wavebox
        .zen-wrapped
        zen
      '';
      mode = "0755";
    };
  };

  # Theme using stylix
  /*
    stylix = {
      enable = true;
      image = /etc/nixos/Wallpapers/catppuccin-wallpaper-1.jpg;
      base16Scheme = {
        base00 = "1e1e2e"; # base
        base01 = "181825"; # mantle
        base02 = "313244"; # surface0
        base03 = "45475a"; # surface1
        base04 = "585b70"; # surface2
        base05 = "cdd6f4"; # text
        base06 = "f5e0dc"; # rosewater
        base07 = "b4befe"; # lavender
        base08 = "f38ba8"; # red
        base09 = "fab387"; # peach
        base0A = "f9e2af"; # yellow
        base0B = "a6e3a1"; # green
        base0C = "94e2d5"; # teal
        base0D = "89b4fa"; # blue
        base0E = "cba6f7"; # mauve
        base0F = "f2cdcd"; # flamingo
      };
    };
  */

  # Environment Variables
  environment.sessionVariables = {
    NIXOS_OZONE_WL = "1";
    XDG_CONFIG_HOME = "/home/karim/.config/";
    STEAM_EXTRA_COMPAT_TOOLS_PATHS = "/home/karim/.steam/root/compatibilitytools.d";
    # NAUTILUS_4_EXTENSION_DIR = lib.mkDefault "${pkgs.nautilus-python}/lib/nautilus/extensions-4";
  };

  environment.variables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
    GTK_THEME = "Catppuccin-Mocha-Standard-Teal-Dark";
    MOZ_ENABLE_WAYLAND = "1";
  };

  environment.pathsToLink = [
    # "/share/nautilus-python/extensions"
    "/share/zsh"
  ];

  # programs.nautilus-open-any-terminal = {
  #   enable = true;
  #   terminal = "kitty";
  # };

  programs.openvpn3.enable = true;

  xdg.portal = {
    enable = true;
    xdgOpenUsePortal = true;
  };

  nixpkgs.config = {
    allowUnfreePredicate =
      pkg:
      builtins.elem (lib.getName pkg) [
        "corefonts"
        "vistafonts"
      ];
    packageOverrides = pkgs: {
      lemminx-maven = pkgs.callPackage ../lemminx-maven/default.nix { };
    };
  };
  # Add Fonts
  fonts.packages = with pkgs; [
    # (nerdfonts.override {
    #   fonts = [
    #     "JetBrainsMono"
    #     "ZedMono"
    #   ];
    # })
    nerd-fonts.zed-mono
    nerd-fonts.jetbrains-mono
    nerd-fonts.hack
    nerd-fonts.fira-code
    corefonts
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.karim = {
    isNormalUser = true;
    home = "/home/karim";
    description = "Karim Salem";
    extraGroups = [
      "wheel"
      "networkmanager"
      "input"
      "audio"
      "corectrl"
      "gamemode"
    ]; # Enable ‘sudo’ for the user.
  };

  # List packages installed systemwide
  environment.systemPackages =
    (with pkgs; [
      # Gaming
      mangohud
      goverlay
      protonup
      lutris
      vulkan-tools
      prismlauncher
      # terminal
      kitty
      git
      gh
      glab
      zsh
      zoxide
      fzf
      atuin
      eza
      bat
      lsof
      tmux
      distrobox
      bear
      # Development
      gnumake
      gcc
      neovim
      jupyter
      lemminx-maven
      # office and windows
      p7zip
      vmware-workstation
      onlyoffice-desktopeditors
      # KDE and utils
      flameshot
      ripgrep
      # nautilus-python
      nautilus
      furmark
      home-manager
      logiops
      # logiops_0_2_3
      openvpn
      kdiskmark
      inputs.zen-browser.packages.${pkgs.system}.default
      catppuccin-cursors.mochaDark
      (pkgs.catppuccin-papirus-folders.override {
        flavor = "mocha";
        accent = "teal";
      })

    ])
    ++ (with pkgs-stable; [
      jetbrains.clion
      jetbrains.idea-ultimate
      jetbrains.rider
      vim
      fastfetch
      wl-clipboard
      unzip
      vlc
      appimage-run
      wget
      busybox
      lsb-release
      gparted
      mediawriter
    ]);

  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    SDL
    SDL2
    SDL2_image
    SDL2_mixer
    SDL2_ttf
    SDL_image
    SDL_mixer
    alsa-lib
    at-spi2-atk
    at-spi2-core
    atk
    bzip2
    cairo
    cups
    curlWithGnuTls
    dbus
    dbus-glib
    desktop-file-utils
    e2fsprogs
    expat
    flac
    fontconfig
    freeglut
    freetype
    fribidi
    fuse
    fuse3
    gdk-pixbuf
    glew110
    glib
    gmp
    gst_all_1.gst-plugins-base
    gst_all_1.gst-plugins-ugly
    gst_all_1.gstreamer
    gtk2
    harfbuzz
    icu
    keyutils.lib
    libGL
    libGLU
    libappindicator-gtk2
    libcaca
    libcanberra
    libcap
    libclang.lib
    libdbusmenu
    libdrm
    libgcrypt
    libgpg-error
    libidn
    libjack2
    libjpeg
    libmikmod
    libogg
    libpng12
    libpulseaudio
    librsvg
    libsamplerate
    libthai
    libtheora
    libtiff
    libudev0-shim
    libusb1
    libuuid
    libvdpau
    libvorbis
    libvpx
    libxcrypt-legacy
    libxkbcommon
    libxml2
    mesa
    nspr
    nss
    openssl
    p11-kit
    pango
    pixman
    python3
    speex
    stdenv.cc.cc
    tbb
    udev
    vulkan-loader
    wayland
    xorg.libICE
    xorg.libSM
    xorg.libX11
    xorg.libXScrnSaver
    xorg.libXcomposite
    xorg.libXcursor
    xorg.libXdamage
    xorg.libXext
    xorg.libXfixes
    xorg.libXft
    xorg.libXi
    xorg.libXinerama
    xorg.libXmu
    xorg.libXrandr
    xorg.libXrender
    xorg.libXt
    xorg.libXtst
    xorg.libXxf86vm
    xorg.libpciaccess
    xorg.libxcb
    xorg.xcbutil
    xorg.xcbutilimage
    xorg.xcbutilkeysyms
    xorg.xcbutilrenderutil
    xorg.xcbutilwm
    xorg.xkeyboardconfig
    xz
    zlib
  ];
}
