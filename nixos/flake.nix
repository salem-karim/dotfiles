{
  description = "My Flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.11";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    plasma-manager = {
      url = "github:nix-community/plasma-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };
    fw-fanctrl = {
      url = "github:TamtamHero/fw-fanctrl/packaging/nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    kwin-effects-forceblur = {
      url = "github:taj-ny/kwin-effects-forceblur";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    lanzaboote = {
      url = "github:nix-community/lanzaboote";
      # url = "github:nix-community/lanzaboote/v0.4.1";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    zen-browser = {
      url = "github:0xc000022070/zen-browser-flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    catppuccin.url = "github:catppuccin/nix";
    # stylix.url = "github:danth/stylix";
  };
  outputs =
    {
      nixpkgs,
      nixpkgs-stable,
      home-manager,
      plasma-manager,
      catppuccin,
      fw-fanctrl,
      lanzaboote,
      ...
    }@inputs:
    let
      lib = nixpkgs.lib;
      system = "x86_64-linux";
      laptopdeskenv = "plasma";
      desktopdeskenv = "plasma";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      pkgs-stable = import nixpkgs-stable {
        inherit system;
        config.allowUnfree = true;
      };
    in
    {
      nixosConfigurations = {
        laptop = lib.nixosSystem {
          inherit system;
          modules = [
            ./common/configuration.nix
            ./laptop/configuration.nix
            ./laptop/hardware-configuration.nix
            ./modules/fw-fanctrl.nix
            (if laptopdeskenv == "gnome" then ./modules/gnome.nix else { })
            (if laptopdeskenv == "plasma" then ./modules/kde.nix else { })
            fw-fanctrl.nixosModules.default
            catppuccin.nixosModules.catppuccin
            lanzaboote.nixosModules.lanzaboote
            # inputs.stylix.nixosModules.stylix
          ];
          specialArgs = {
            inherit pkgs-stable inputs laptopdeskenv;
          };
        };
        desktop = lib.nixosSystem {
          inherit system;
          modules = [
            ./common/configuration.nix
            ./desktop/configuration.nix
            ./desktop/hardware-configuration.nix
            (if desktopdeskenv == "gnome" then ./modules/gnome.nix else { })
            (if desktopdeskenv == "plasma" then ./modules/kde.nix else { })
            (if desktopdeskenv == "hyprland" then ./modules/hyprland.nix else { })
            catppuccin.nixosModules.catppuccin
            lanzaboote.nixosModules.lanzaboote
            # inputs.stylix.nixosModules.stylix
          ];
          specialArgs = {
            inherit pkgs-stable inputs desktopdeskenv;
          };
        };
      };

      homeConfigurations = {
        laptop = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          modules = [
            ./common/home.nix
            ./laptop/home.nix
            (if laptopdeskenv == "gnome" then ./modules/gnome-home.nix else { })
            (if laptopdeskenv == "plasma" then ./modules/kde-home.nix else { })
            plasma-manager.homeManagerModules.plasma-manager
            catppuccin.homeManagerModules.catppuccin
          ];
          extraSpecialArgs = {
            inherit pkgs-stable laptopdeskenv;
          };
        };

        desktop = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          modules = [
            ./common/home.nix
            ./desktop/home.nix
            (if desktopdeskenv == "gnome" then ./modules/gnome-home.nix else { })
            (if desktopdeskenv == "plasma" then ./modules/kde-home.nix else { })
            plasma-manager.homeManagerModules.plasma-manager
            catppuccin.homeManagerModules.catppuccin
          ];
          extraSpecialArgs = {
            inherit pkgs-stable desktopdeskenv;
          };
        };
      };
    };
}
