set-option -ga terminal-overrides ",xterm-kitty:Tc:RGB:xterm-keys:*"
set -g default-terminal "xterm-kitty"
set -as terminal-features ",xterm-kitty*:RGB"
set-option -g status-position top
set-option -g xterm-keys on

#IMPORTANT DO NOT TOUCH
set -g extended-keys on
#######################

# rebind main key: C-space
unbind C-b
set -g prefix C-space
bind C-space send-prefix

bind h split-window -v -c "#{pane_current_path}"
bind v split-window -h -c "#{pane_current_path}"
bind c new-window -c "#{pane_current_path}"
bind x kill-pane
bind X kill-window
bind b join-pane

# Resize pane bindings using Alt + Shift + h/j/k/l without prefix
bind -n M-H resize-pane -L   # Alt + Shift + H to resize left
bind -n M-J resize-pane -D   # Alt + Shift + J to resize down
bind -n M-K resize-pane -U   # Alt + Shift + K to resize up
bind -n M-L resize-pane -R   # Alt + Shift + L to resize right

# bind Ctrl+Shift+key to keycode
bind -n C-S-H send-keys Escape "[72;6u"
bind -n C-S-J send-keys Escape "[74;6u"
bind -n C-S-K send-keys Escape "[75;6u"
bind -n C-S-L send-keys Escape "[76;6u"


# Shift+Enter
bind -n S-Enter send-keys Escape "[13;2u"
# Control+Enter
bind -n C-Enter send-keys Escape "[13;5u"
# Control+Shift+Enter
bind -n C-S-Enter send-keys Escape "[13;6u"

bind S source-file ~/.config/tmux/tmux.conf

set -g mouse on
set -g base-index 1
set -g mode-keys vi
set -g status-keys vi
set -s escape-time 500
setw -g pane-base-index 1
set -g history-limit 2000
set -g renumber-windows on
setw -g clock-mode-style 24
setw -g aggressive-resize off
set-window-option -g mode-keys vi

set -g @catppuccin_flavor 'mocha'
set -g @catppuccin_window_status_style "rounded"
run '~/.config/tmux/plugins/tmux/catppuccin.tmux'
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'christoomey/vim-tmux-navigator'
set -g @plugin 'catppuccin/tmux#v2.1.0'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'alexwforsythe/tmux-which-key'

set -g @vim_navigator_mapping_left "C-Left C-h" 
set -g @vim_navigator_mapping_right "C-Right C-l"
set -g @vim_navigator_mapping_up "C-k"
set -g @vim_navigator_mapping_down "C-j"

set -g @continuum-restore on

bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi C-v send-keys -X rectangle-toggle
bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel

set -g @catppuccin_date_time_text " %d.%m %H:%M"
set -g status-right-length 100
set -g status-left-length 100
set -g status-left "#{E:@catppuccin_status_session}"
set -g status-right "#{E:@catppuccin_status_directory}"
set -ag status-right "#{E:@catppuccin_status_date_time}"


run '~/.config/tmux/plugins/tpm/tpm'
