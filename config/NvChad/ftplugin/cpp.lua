vim.api.nvim_set_keymap(
  "v",
  "<leader>im",
  [[<cmd>lua require("plugins.cpptools.cpptools").create_func_def()<CR>]],
  { desc = "Create Function Def", silent = true }
)
vim.api.nvim_set_keymap(
  "n",
  "<leader>im",
  [[<cmd>lua require("plugins.cpptools.cpptools").create_func_def()<CR>]],
  { desc = "Create Function Def", silent = true }
)
vim.api.nvim_set_keymap(
  "n",
  "<leader>if",
  [[<cmd>lua require("plugins.cpptools.cpptools").create_file()<CR>]],
  { desc = "Create File", silent = true }
)
local map = vim.keymap.set
map("n", "<leader>cc", ":CMakeClean<cr>", { desc = "CMake Clean", silent = true })
map("n", "<leader>cg", ":CMakeGenerate<cr>", { desc = "CMake Generate", silent = true })
map("n", "<leader>cb", ":CMakeBuild<cr>", { desc = "CMake Build", silent = true })
map("n", "<leader>cr", ":CMakeRun<cr>", { desc = "CMake Run", silent = true })
map("n", "<leader>cd", ":CMakeDebug<cr>", { desc = "CMake Debug", silent = true })
map("n", "<leader>ct", ":CMakeRunTest<cr>", { desc = "CMake Run Test", silent = true })
map("n", "<leader>csr", ":CMakeSelectLaunchTarget<cr>", { desc = "CMake Select Run Tareget", silent = true })
map("n", "<leader>csb", ":CMakeSelectBuildTarget<cr>", { desc = "CMake Select Build Target", silent = true })
map("n", "<leader>csc", ":CMakeSelectCwd<cr>", { desc = "CMake Select CWD", silent = true })
map("n", "<leader>ih", "<cmd>ClangdToggleInlayHints<CR>", { desc = "Toggle Inlay Hints", silent = true })
map("n", "<leader>csf", "<cmd>ClangdSwitchSourceHeader<CR>", { desc = "Switch to Header/Source File", silent = true })
map("n", "<leader>cth", "<cmd>ClangdTypeHierarchy<CR>", { desc = "Show Type Hierarchy", silent = true })
map("n", "<leader>csa", "<cmd>ClangdAST<CR>", { desc = "Show Abstract Syntax Tree", silent = true })
