-- Define the function globally so it can be accessed by keymaps and autocmds
_G.format_sqlfluff = function()
  -- Save the current buffer position
  local current_pos = vim.fn.getpos "."
  -- Get current buffer contents
  local lines = vim.api.nvim_buf_get_lines(0, 0, -1, false)
  local content = table.concat(lines, "\n")
  -- Create temp buffers for stdout and stderr
  local stdout = {}
  local stderr = {}
  -- Configure the job
  local job = vim.fn.jobstart({ "sqlfluff", "format", "--dialect", "postgres", "-" }, {
    stdout_buffered = true,
    stderr_buffered = true,
    on_stdout = function(_, data)
      if data then
        for _, line in ipairs(data) do
          if line ~= "" then
            table.insert(stdout, line)
          end
        end
      end
    end,
    on_stderr = function(_, data)
      if data then
        for _, line in ipairs(data) do
          if line ~= "" then
            table.insert(stderr, line)
          end
        end
      end
    end,
    on_exit = function(_, exit_code)
      if exit_code == 0 and #stdout > 0 then
        -- Update buffer with formatted content
        vim.schedule(function()
          vim.api.nvim_buf_set_lines(0, 0, -1, false, stdout)
          vim.fn.setpos(".", current_pos)
        end)
      elseif #stderr > 0 then
        vim.schedule(function()
          vim.notify("SQLFluff error: " .. table.concat(stderr, "\n"), vim.log.levels.ERROR)
        end)
      end
    end,
  })

  -- Send content to SQLFluff through stdin
  if job then
    vim.fn.chansend(job, content)
    vim.fn.chanclose(job, "stdin")
  else
    vim.notify("Failed to start SQLFluff", vim.log.levels.ERROR)
  end
end

-- Map <leader>fm to run formatting
vim.api.nvim_set_keymap("n", "<leader>fm", "<cmd>lua _G.format_sqlfluff()<CR>", { noremap = true, silent = true })
