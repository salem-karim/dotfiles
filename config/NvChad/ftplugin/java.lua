local home = vim.env.HOME
local openjdk_path = vim.env.JAVA_HOME .. "/lib/openjdk/"
local lombok_path = vim.env.LOMBOK_HOME .. "/share/java/lombok.jar"
local masonpkgs_path = home .. "/.local/share/nvim/mason/packages"
local jdebug_path = masonpkgs_path .. "/java-debug-adapter"
local jtest_path = masonpkgs_path .. "/java-test"
local jdtls = require "jdtls"
local project_name = vim.fn.fnamemodify(vim.fn.getcwd(), ":p:h:t")
local workspace_dir = home .. "/jdtls-workspace/" .. project_name
local nvattach = require("configs.lsp").on_attach

-- Ensure bundles are properly loaded with error checking
local function get_bundles()
  local bundles = {}

  -- Debug bundles
  local debugger_path = vim.fn.glob(jdebug_path .. "/extension/server/com.microsoft.java.debug.plugin-*.jar", true)
  if debugger_path ~= "" then
    table.insert(bundles, debugger_path)
  end

  -- Test bundles
  local test_bundle_path = jtest_path .. "/extension/server/*.jar"
  local test_bundles = vim.split(vim.fn.glob(test_bundle_path, true), "\n")
  vim.list_extend(bundles, test_bundles)

  return bundles
end

local config = {
  cmd = {
    "java",
    "-Declipse.application=org.eclipse.jdt.ls.core.id1",
    "-Dosgi.bundles.defaultStartLevel=4",
    "-Declipse.product=org.eclipse.jdt.ls.core.product",
    "-Dlog.protocol=true",
    "-Dlog.level=ALL",
    "-javaagent:" .. lombok_path,
    "-Xmx2g",
    "--add-modules=ALL-SYSTEM",
    "--add-opens",
    "java.base/java.util=ALL-UNNAMED",
    "--add-opens",
    "java.base/java.lang=ALL-UNNAMED",
    "-jar",
    vim.fn.glob(home .. "/.local/share/nvim/mason/share/jdtls/plugins/org.eclipse.equinox.launcher_*.jar"),
    "-configuration",
    home .. "/.local/share/nvim/mason/packages/jdtls/config_linux",
    "-data",
    workspace_dir,
  },

  root_dir = require("jdtls.setup").find_root { ".git", "mvnw", "pom.xml", "build.gradle" },

  settings = {
    java = {
      home = openjdk_path,
      eclipse = {
        downloadSources = true,
      },
      configuration = {
        updateBuildConfiguration = "interactive",
        runtimes = {
          {
            name = "JavaSE-21",
            path = openjdk_path,
          },
        },
      },
      maven = {
        downloadSources = true,
      },
      implementationsCodeLens = {
        enabled = true,
      },
      referencesCodeLens = {
        enabled = true,
      },
      references = {
        includeDecompiledSources = true,
      },
      signatureHelp = { enabled = true },
      -- Fixed formatter configuration
      format = {
        enabled = true,
        settings = {
          -- Using raw URL instead of GitHub web URL
          url = "https://raw.githubusercontent.com/google/styleguide/gh-pages/intellij-java-google-style.xml",
          profile = "GoogleStyle",
        },
      },
    },
    completion = {
      favoriteStaticMembers = {
        "org.hamcrest.MatcherAssert.assertThat",
        "org.hamcrest.Matchers.*",
        "org.hamcrest.CoreMatchers.*",
        "org.junit.jupiter.api.Assertions.*",
        "java.util.Objects.requireNonNull",
        "java.util.Objects.requireNonNullElse",
        "org.mockito.Mockito.*",
      },
    },
    extendedClientCapabilities = jdtls.extendedClientCapabilities,
    sources = {
      organizeImports = {
        starThreshold = 9999,
        staticStarThreshold = 9999,
      },
    },
  },

  init_options = {
    bundles = get_bundles(),
    extendedClientCapabilities = {
      progressReportProvider = true,
      classFileContentsSupport = true,
      generateToStringPromptSupport = true,
      hashCodeEqualsPromptSupport = true,
      advancedExtractRefactoringSupport = true,
      advancedOrganizeImportsSupport = true,
      generateConstructorsPromptSupport = true,
      generateDelegateMethodsPromptSupport = true,
      moveRefactoringSupport = true,
      overrideMethodsPromptSupport = true,
      resolveAdditionalTextEditsSupport = true,
    },
  },

  capabilities = require("cmp_nvim_lsp").default_capabilities(),

  on_attach = function(client, bufnr)
    jdtls.setup_dap { hotcodereplace = "auto" }
    require("jdtls.dap").setup_dap_main_class_configs()
    require("jdtls.setup").add_commands()
    nvattach(client, bufnr)
  end,
}

-- Start the server
jdtls.start_or_attach(config)

local hl = vim.api.nvim_set_hl
local colors = require "colors"

hl(0, "@lsp.type.modifier.java", { fg = colors.mocha.mauve })
hl(0, "@module", { fg = colors.base_30.lavender })
hl(0, "@variable", { fg = colors.mocha.lavender })

local function runScript()
  local cwd = require("jdtls.setup").find_root { "pom.xml" }
  -- Assumes run script name
  local cmd = "./run.sh"
  local runTerm = require("toggleterm.terminal").Terminal:new {
    cmd = cmd,
    dir = cwd,
    direction = "horizontal",
    close_on_exit = false,
    on_open = function(term)
      vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>", { noremap = true, silent = true })
    end,
    on_close = function(term)
      if term:is_open() then
        term:shutdown() -- Terminates the process in the terminal buffer
      end
    end,
  }
  runTerm:toggle()
end

local map = vim.keymap.set
local function opts(desc)
  return { desc = "LSP " .. desc, silent = true }
end

local function mvn(desc)
  return { desc = "Maven " .. desc, silent = true }
end

-- Maven
map("n", "<leader>mb", "<Cmd>MavenExec<CR>compile", mvn "Build Maven Project")
map("n", "<leader>mc", "<Cmd>MavenExec<CR>clean", mvn "Clean Maven Project")
map("n", "<leader>mp", "<Cmd>MavenExec<CR>package", mvn "Package Maven Project")
map("n", "<leader>mm", "<Cmd>Maven<CR>", mvn "open Maven tool")

-- JDTLS mappings
map("n", "<leader>co", "<Cmd>lua require'jdtls'.organize_imports()<CR>", opts "Organize Imports")
map("n", "<leader>cev", "<Cmd>lua require('jdtls').extract_variable()<CR>", opts "Extract Variable")
map("v", "<leader>cev", "<Esc><Cmd>lua require('jdtls').extract_variable(true)<CR>", opts "Extract Variable")
map("n", "<leader>cec", "<Cmd>lua require('jdtls').extract_constant()<CR>", opts "Extract Constant")
map("v", "<leader>cec", "<Esc><Cmd>lua require('jdtls').extract_constant(true)<CR>", opts "Extract Constant")
map({ "v", "x" }, "<leader>cem", "<Esc><Cmd>lua require('jdtls').extract_method(true)<CR>", opts "Extract Method")
map({ "v", "x" }, "<leader>cgt", "<Esc><Cmd>lua require('jdtls.tests').generate()<CR>", opts "Generate Tests")
map("n", "<leader>cs", "<Cmd>lua require'jdtls.tests'.goto_subjects()<CR>", opts "Jump to Tests or subject")
map("n", "<leader>gu", "<Cmd>lua require('jdtls').update_projects_config()<CR>", opts "Update Projects config")
map("n", "<leader>tc", "<Cmd>lua require('jdtls').test_class()<CR>", opts "Test Whole Class")
map("n", "<leader>tm", "<Cmd>lua require('jdtls').test_nearest_method()<CR>", opts "Test nearest Method")
-- Runs run.sh in ToggleTerm
map("n", "<leader>cr", runScript, opts "Run Project Script")
