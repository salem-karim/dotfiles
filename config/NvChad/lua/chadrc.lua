local integrations = {
  "mason",
  "lsp",
  "dap",
  "cmp",
  "todo",
  "notify",
  "syntax",
  "statusline",
  "devicons",
  "trouble",
}
local sl = require "statusline"

local colors = require "colors"

local M = {

  base46 = {
    theme = "catppuccin",
    transparency = true,
    integrations = integrations,
    hl_override = {
      Comment = { italic = true },
      SpecialChar = { fg = colors.base_30.pink },
      Include = { fg = colors.mocha.mauve },
      LineNr = { fg = colors.mocha.overlay_0 },
      DiagnosticUnnecessary = { fg = colors.mocha.overlay_0 },
      ["@comment"] = { fg = colors.mocha.overlay_0, italic = true },
      ["@constant"] = { fg = colors.base_30.cyan },
      ["@module"] = { fg = colors.base_30.baby_pink },
      ["@character"] = { fg = colors.base_30.green },
      ["@operator"] = { fg = colors.base_30.cyan, italic = true },
      ["@punctuation.delimiter"] = { fg = colors.rosewater },
      ["@variable.member"] = { fg = colors.mocha.teal },
      ["@variable.builtin"] = { fg = colors.mauve },
      ["@keyword.exception"] = { fg = colors.mauve },
      ["@type.builtin"] = { fg = colors.mauve },
      ["@string.escape"] = { fg = colors.base_30.pink },
      ["@punctuation.bracket"] = { fg = colors.subtext_0 },
      ["@tag"] = { fg = colors.base_30.purple },
      ["@tag.delimiter"] = { fg = colors.lavender },
    },
  },

  ui = {

    statusline = sl,

    tabufline = {
      -- order = { "buffers", "tabs", "btns", "treeOffset" },
      order = { "buffers" },
    },

    cmp = {
      style = "default",
      -- style = "atom_colored",
      -- style = "flat_light",
      -- style = "flat_dark",
      lspkind_text = true,
      icons = true,
    },
  },

  term = {
    float = {
      col = 0.05,
      row = 0.05,
      width = 0.8,
      height = 0.8,
    },
  },
}
return M
