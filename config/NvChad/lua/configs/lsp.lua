local M = {}
local map = vim.keymap.set

local function detect_maven_pom(bufnr)
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, 5, false)
  for _, line in ipairs(lines) do
    if line:match "<project" then
      return true
    end
  end
  return false
end

-- export on_attach & capabilities
M.on_attach = function(client, bufnr)
  if detect_maven_pom(bufnr) then
    client.server_capabilities.completionProvider.resolveProvider = false
    map("n", "<leader>mm", "<Cmd>Maven<CR>", { desc = "Maven " .. "open Maven tool", silent = true })
  else
    local function opts(desc)
      return { buffer = bufnr, desc = "LSP " .. desc }
    end

    map("n", "gD", function()
      Snacks.picker.lsp_declarations()
    end, opts "Go to declaration")
    map("n", "gd", function()
      Snacks.picker.lsp_definitions()
    end, opts "Go to definition")
    map("n", "gi", function()
      Snacks.picker.lsp_implementation()
    end, opts "Go to implementation")
    map("n", "<leader>sh", vim.lsp.buf.signature_help, opts "Show signature help")
    map("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, opts "Add workspace folder")
    map("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, opts "Remove workspace folder")
    map("n", "<leader>ws", function()
      Snacks.picker.lsp_workspace_symbols()
    end, opts "List Workspace Symbols")
    map("n", "<leader>gs", function()
      Snacks.picker.lsp_symbols()
    end, opts "List Symbols")

    map("n", "gr", function()
      Snacks.picker.lsp_references()
    end, opts "show References")
    map("n", "<leader>D", function()
      Snacks.picker.lsp_type_definitions()
    end, opts "show TypeDefinition")
    map("n", "<leader>wl", function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts "List workspace folders")

    map("n", "<leader>ra", require "nvchad.lsp.renamer", opts "NvRenamer")
    map({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, opts "Code action")

    -- Inlay hints
    if client.server_capabilities.inlayHintProvider then
      local inlay_hints_autocmds_enabled = false
      local augroup = vim.api.nvim_create_augroup("lsp_augroup", { clear = true })

      local function set_inlay_hints_autocmds(enable)
        if enable then
          vim.api.nvim_create_autocmd({ "InsertEnter", "InsertLeave" }, {
            group = augroup,
            callback = function(event)
              vim.lsp.inlay_hint.enable(event.event == "InsertEnter")
            end,
          })
        else
          vim.api.nvim_clear_autocmds { group = augroup }
          vim.lsp.inlay_hint.enable(false) -- Ensure hints are off when disabled
        end
      end

      -- Initialize the autocmds
      set_inlay_hints_autocmds(false)

      -- Keymap to toggle inlay hints autocmds globally
      map("n", "<leader>th", function()
        inlay_hints_autocmds_enabled = not inlay_hints_autocmds_enabled
        set_inlay_hints_autocmds(inlay_hints_autocmds_enabled)
        print("Inlay hints autocmds " .. (inlay_hints_autocmds_enabled and "enabled" or "disabled"))
      end, { buffer = bufnr, desc = "LSP: Toggle inlay hints autocmds", silent = true })
    end
  end
end

-- disable semanticTokens
M.on_init = function(client, _)
  if client.supports_method "textDocument/semanticTokens" then
    client.server_capabilities.semanticTokensProvider = nil
  end
end

M.capabilities = vim.lsp.protocol.make_client_capabilities()

M.capabilities.textDocument.completion.completionItem = {
  documentationFormat = { "markdown", "plaintext" },
  snippetSupport = true,
  preselectSupport = true,
  insertReplaceSupport = true,
  labelDetailsSupport = true,
  deprecatedSupport = true,
  commitCharactersSupport = true,
  tagSupport = { valueSet = { 1 } },
  resolveSupport = {
    properties = {
      "documentation",
      "detail",
      "additionalTextEdits",
    },
  },
}

return M
