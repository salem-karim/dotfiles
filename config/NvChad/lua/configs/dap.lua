-- configs/dap.lua
local M = {}

function M.setup()
  local dap = require "dap"
  local dapui = require "dapui"

  -- UI configuration
  local icons = {
    Stopped = { "󰁕 ", "DiagnosticWarn", "DapStoppedLine" },
    Breakpoint = "",
    BreakpointCondition = "",
    BreakpointRejected = { "", "DiagnosticError" },
    LogPoint = ".>",
  }

  vim.fn.sign_define("DapBreakpoint", { text = icons.Breakpoint, texthl = "DapBreakpoint", linehl = "", numhl = "" })
  vim.fn.sign_define(
    "DapBreakpointCondition",
    { text = icons.BreakpointCondition, texthl = "DapBreakpointCondition", linehl = "", numhl = "" }
  )
  vim.fn.sign_define("DapLogPoint", { text = icons.LogPoint, texthl = "DapLogPoint", linehl = "", numhl = "" })
  vim.fn.sign_define(
    "DapStopped",
    { text = icons.Stopped[1], texthl = "DapStopped", linehl = "DapStoppedLine", numhl = "DapStopped" }
  )
  vim.fn.sign_define(
    "DapBreakpointRejected",
    { text = icons.BreakpointRejected[1], texthl = "DapBreakpointRejected", linehl = "", numhl = "" }
  )

  -- LLDB configuration for C/C++/Rust
  local lldb_config = {
    name = "Launch LLDB",
    type = "lldb",
    request = "launch",
    program = function()
      return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
    end,
    cwd = "${workspaceFolder}",
    stopOnEntry = false,
    args = {},
    runInTerminal = false,
    env = function()
      local variables = {}
      return variables
    end,
  }

  dap.adapters.lldb = {
    type = "executable",
    command = "/home/karim/.local/share/nvim/mason/bin/codelldb",
    name = "lldb",
  }

  dap.configurations.c = { lldb_config }
  dap.configurations.cpp = { lldb_config }
  dap.configurations.rust = { lldb_config }

  -- -- Node.js configuration
  -- dap.adapters["pwa-node"] = {
  --   type = "server",
  --   host = "127.0.0.1",
  --   port = 8123,
  --   executable = {
  --     command = "js-debug-adapter",
  --   },
  -- }

  local node_config = {
    type = "pwa-node",
    request = "launch",
    name = "Launch file",
    program = "${file}",
    cwd = "${workspaceFolder}",
    runtimeExecutable = "node",
    sourceMaps = true,
    protocol = "inspector",
    console = "integratedTerminal",
  }

  for _, language in ipairs { "typescript", "javascript" } do
    dap.configurations[language] = { node_config }
  end

  -- Godot configuration
  dap.adapters.godot = {
    type = "server",
    host = "127.0.0.1",
    port = 6006,
  }

  dap.configurations.gdscript = {
    {
      type = "godot",
      request = "launch",
      name = "Launch Godot Scene",
      project = "${workspaceFolder}",
      debug_collisions = false, -- Optional: Show collision shapes when debugging
      debug_navigation = false, -- Optional: Show navigation polygons when debugging
      debug_paths = false, -- Optional: Show path finding debug when debugging
      scene = function()
        return vim.fn.input("Path to scene: ", vim.fn.getcwd() .. "/", "file")
      end,
    },
  }

  -- C# config with Omnisharp

  dap.adapters["netcoredbg"] = {
    type = "executable",
    command = "/home/karim/.nix-profile/bin/netcoredbg",
    args = { "--interpreter=vscode" },
    options = {
      detached = false,
    },
  }

  for _, lang in ipairs { "cs", "fsharp", "vb" } do
    dap.configurations[lang] = {
      {
        type = "netcoredbg",
        name = "Launch file",
        request = "launch",
        program = function()
          return vim.fn.input("Path to dll: ", vim.fn.getcwd() .. "/", "file")
        end,
        cwd = "${workspaceFolder}",
      },
    }
  end

  dap.configurations.java = {
    {
      name = "Debug Launch (4GB)",
      type = "java",
      request = "launch",
      vmArgs = "" .. "-Xmx4g ",
    },
    {
      name = "Debug Attach (8000)",
      type = "java",
      request = "attach",
      hostName = "127.0.0.1",
      port = 8000,
    },
    {
      name = "Debug Attach (5005)",
      type = "java",
      request = "attach",
      hostName = "127.0.0.1",
      port = 5005,
    },
  }

  -- Keymaps
  local function set_conditional_breakpoint()
    dap.set_breakpoint(vim.fn.input "Breakpoint condition: ")
  end
  local map = vim.keymap.set
  -- Setup keymaps
  map("n", "<leader>db", dap.toggle_breakpoint, { desc = "Debug: Toggle Breakpoint" })
  map("n", "<leader>dr", dap.continue, { desc = "Debug: Start/Continue" })
  map("n", "<leader>si", dap.step_into, { desc = "Debug: Step Into" })
  map("n", "<leader>so", dap.step_over, { desc = "Debug: Step Over" })
  map("n", "<leader>sO", dap.step_out, { desc = "Debug: Step Out" })
  map("n", "<leader>dB", set_conditional_breakpoint, { desc = "Debug: Set Conditional Breakpoint" })
  map("n", "<leader>dlr", dap.repl.open, { desc = "Debug: Open REPL" })
  map("n", "<leader>dlp", function()
    dap.set_breakpoint(nil, nil, vim.fn.input "Log point message: ")
  end, { desc = "Debug: Set Log Point" })

  -- Auto-open DAP UI
  dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open()
  end
  dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close()
  end
  dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close()
  end
end

return M
