local options = {
  formatters_by_ft = {
    lua = { "stylua" },
    css = { "biome" },
    html = { "biome" },
    typescript = { "biome" },
    javascript = { "biome" },
    c = { "clang-format" },
    cpp = { "clang_format" },
    cmake = { "cmake_format" },
    zig = { "zigfmt" },
    rust = { "rustfmt" },
    go = { "gofmt" },
    python = { "yapf" },
    cs = { "csharpier" },
    nix = { "nixpkgs_fmt" },
  },

  formatters = {
    biome = { args = { "format" } },
    yapf = { args = { "--style={indent_width:2}" } },
    clang_format = { command = "/home/karim/.nix-profile/bin/clang-format" },
    nixpkgs_fmt = { command = "/home/karim/.nix-profile/bin/nixfmt" },
    csharpier = { command = "dotnet-csharpier", args = { "--write-stdout" } },
    rustfmt = { command = "rustfmt", args = { "--config", "tab_spaces=2" } },
  },

  format_on_save = {
    -- These options will be passed to conform.format()
    timeout_ms = 1000,
    lsp_fallback = true,
  },
}

return options
