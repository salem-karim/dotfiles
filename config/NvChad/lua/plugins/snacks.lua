vim.api.nvim_set_hl(0, "SnacksIndentScope", { fg = require("colors").base_30.lavender })

return {
  "folke/snacks.nvim",
  lazy = false,
  opts = {
    notifier = {
      enabled = true,
      top_down = false,
    },
    indent = { enabled = true },
    input = { enabled = true },
    image = { enabled = true },
    dashboard = { enabled = true },
    picker = {
      enabled = true,
      matcher = {
        frecency = true,
      },
      win = {
        input = {
          keys = {
            ["<A-j>"] = { "list_down", mode = { "i", "n" } },
            ["<A-k>"] = { "list_up", mode = { "i", "n" } },
            ["<C-S-H>"] = { "toggle_hidden", mode = { "i", "n" } },
            ["<A-i>"] = { "toggle_ignored", mode = { "i", "n" } },
          },
        },
      },
    },
  },
  keys = {
    {
      "<leader>:",
      function()
        Snacks.picker.command_history()
      end,
      desc = "Command History",
    },
    {
      "<leader>n",
      function()
        Snacks.picker.notifications()
      end,
      desc = "Notification History",
    },
    {
      "<leader>z",
      function()
        Snacks.zen()
      end,
      desc = "Toggle Zen Mode",
    },
    {
      "<leader>dn",
      function()
        Snacks.notifier.hide()
      end,
      desc = "Dismiss All Notifications",
    },
    {
      "<leader>ff",
      function()
        Snacks.picker.files()
      end,
      desc = "Find Files",
    },
    {
      "<leader>fa",
      function()
        Snacks.picker.files { hidden = true, ignored = true }
      end,
      desc = "Find All Files",
    },
    {
      "<leader>fw",
      function()
        Snacks.picker.grep()
      end,
      desc = "Grep Search",
    },
    {
      "<leader>fb",
      function()
        Snacks.picker.buffers()
      end,
      desc = "Pick Buffers",
    },
    {
      "<leader>fm",
      function()
        Snacks.picker.marks()
      end,
      desc = "Find Marks",
    },
    {
      "<leader>fc",
      function()
        Snacks.picker.files { cwd = vim.fn.stdpath "config" }
      end,
      desc = "Find Config File",
    },
    {
      "<leader>ds",
      function()
        Snacks.picker.diagnostics()
      end,
      desc = "Diagnostics",
    },
    {
      "<leader>df",
      function()
        Snacks.picker.diagnostics_buffer()
      end,
      desc = "Buffer Diagnostics",
    },
    {
      "<leader>sk",
      function()
        Snacks.picker.keymaps()
      end,
      desc = "Search Keymaps",
    },
    {
      "<leader>e",
      function()
        Snacks.explorer { layout = { layout = { position = "right" } } }
      end,
      desc = "File Explorer",
    },
    {
      "<leader>cR",
      function()
        Snacks.rename.rename_file()
      end,
      desc = "Rename File",
    },
  },
}
