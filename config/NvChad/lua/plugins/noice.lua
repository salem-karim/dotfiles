return { {
  "folke/noice.nvim",
  event = "VeryLazy",
  opts = {
    views = {
      mini = {
        focusable = false,
        win_options = {
          winblend = 0,
          winhighlight = {
            NoiceLspProgressTitle = "@comment",
          },
        },
      },
    },
    routes = {
      {
        filter = { event = "notify", find = "No information available" },
        opts = { skip = true },
      },
      {
        filter = {
          event = "notify",
          find = "multiple different client offset_encodings detected"
        },
        opts = { skip = true },
      },
      {
        view = "mini",
        filter = {
          event = "msg_showmode",
          any = {
            { find = "recording" },
          }
        }
      }
    },
    presets = {
      lsp_doc_border = true,
    },
    lsp = {
      signature = {
        enabled = false,
      },
      hover = {
        enabled = false,
      },
    },
  },
  dependencies = {
    "MunifTanjim/nui.nvim",
  },
},

  {
    "folke/which-key.nvim",
    keys = { "<leader>", "<c-w>", '"', "'", "`", "c", "v", "g" },
    cmd = "WhichKey",
    config = function()
      dofile(vim.g.base46_cache .. "whichkey")
      local opts = {
        spec = {
          { 'K',         group = 'LSP: On Hover' },
          { '<leader>c', group = '[C]ode',               mode = { 'n', 'x' } },
          { '<leader>d', group = '[D]ebug/[D]iagnostics' },
          { '<leader>r', group = '[R]efactor' },
          { '<leader>w', group = '[W]orkspace' },
          { '<leader>t', group = '[T]oggle' },
          { '<leader>f', group = '[F]ind' },
          { '<leader>l', group = '[L]azy' },
          { '<leader>s', group = '[S]earch/[S]tep' },
        },
      }
      require("which-key").setup(opts)
    end
  },
}
