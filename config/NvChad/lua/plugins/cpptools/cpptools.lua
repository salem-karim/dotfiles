local parsers = require "nvim-treesitter.parsers"
local utils = require "plugins.cpptools.utils"
local error = vim.log.levels.ERROR
local info = vim.log.levels.INFO

-- ===========================create file=====================================================

local function _cf(file_name, file_type, path)
  local file
  local file_path = path .. file_name

  if file_type then
    file_path = file_path .. "." .. file_type
  end

  if utils.hasfile(file_path) then
    vim.api.nvim_command("edit " .. file_path)
    return
  end

  file = io.open(file_path, "a+")
  if not file then
    vim.notify("create file error!", error, {
      title = "error",
    })
    return
  end

  if file_type == "h" or file_type == "hpp" then
    file:write "#pragma once\n"
    file:write("#ifndef _" .. string.upper(file_name) .. "_" .. string.upper(file_type) .. "_\n")
    file:write("#define _" .. string.upper(file_name) .. "_" .. string.upper(file_type) .. "_\n")
    file:write "\n"
    file:write "\n"
    file:write "#endif"
  elseif file_type == "c" then
    if utils.hasfile(path .. file_name .. ".h") then
      file:write([[#include "]] .. file_name .. ".h" .. [["]] .. "\n")
    end
  elseif file_type == "cpp" then
    if utils.hasfile(path .. file_name .. ".hpp") then
      file:write([[#include "]] .. file_name .. ".hpp" .. [["]] .. "\n")
    elseif utils.hasfile(path .. file_name .. ".h") then
      file:write([[#include "]] .. file_name .. ".h" .. [["]] .. "\n")
    end
  end

  io.close(file)
  vim.notify(file_path, info, {
    title = "Create file!",
  })
  vim.api.nvim_command("edit " .. file_path)
end

local function create_file()
  local current_dir = vim.fn.expand "%:p:h"
  current_dir = string.gsub(current_dir, "\\", "/")
  current_dir = current_dir .. "/"

  vim.ui.input({ prompt = "Create file ", default = current_dir, completion = "dir" }, function(input)
    if not input then
      print "No input!"
      return
    end

    input = input:gsub("\\", "/")
    -- print("Input: ", input)

    utils.clear_cmd()

    if input == current_dir then
      print "Input is current directory!"
      return
    end

    local path = assert(input:match "(.*/)")

    -- if not file_name then
    -- 	print("No file name provided!")
    -- 	return
    -- end

    -- Extract file type if present
    local file_name = string.match(input, ".+/([^/]+)$")
    local file_type = nil
    if file_name then
      file_type = file_name:match "%.([^%.]+)$"
      if file_type then
        -- If file type is present, remove it from file_name
        file_name = assert(file_name:match "(.+)%..+")
      end
    end

    if file_name then
      -- Create file
      if utils.isdir(path) then
        _cf(file_name, file_type, path)
      else
        print("Creating directory: ", path)
        utils.makedirs(path)
        _cf(file_name, file_type, path)
      end
    else
      -- Create directory
      print("Creating directory: ", path)
      utils.makedirs(path)
    end
  end)
end

-- ==========================================================================================

-- ====================================create func def=======================================

local function traverse_node(node, n_info, c_info)
  if node:type() == "identifier" and node:parent():type() == "namespace_definition" then
    local namespace = {}
    local b_r, b_c, e_r, e_c = node:range()
    local namespace_name = vim.api.nvim_buf_get_lines(0, b_r, e_r + 1, false)[1]:sub(b_c, e_c)
    b_r, b_c, e_r, e_c = node:parent():range()
    local range = { b_r + 1, e_r + 1 }
    namespace[1] = namespace_name
    namespace[2] = range[1]
    namespace[3] = range[2]
    table.insert(n_info, namespace)
  elseif node:type() == "type_identifier" and node:parent():type() == "class_specifier" then
    local class = {}
    local b_r, b_c, e_r, e_c = node:range()
    local class_name = vim.api.nvim_buf_get_lines(0, b_r, e_r + 1, false)[1]:sub(b_c, e_c)
    b_r, b_c, e_r, e_c = node:parent():range()
    local range = { b_r + 1, e_r + 1 }
    class[1] = class_name
    class[2] = range[1]
    class[3] = range[2]
    table.insert(c_info, class)
  end
  for child_node in node:iter_children() do
    traverse_node(child_node, n_info, c_info)
  end
end

local function get_cpp_header_info(bufnr, lang)
  local parser = parsers.get_parser(bufnr, lang)
  local root = parser:parse()[1]:root()
  local namespace_info = {}
  local class_info = {}
  traverse_node(root, namespace_info, class_info)

  return namespace_info, class_info
end

local function get_select_lines(buf)
  -- local lines_begin, lines_end = vim.fn.getpos("'<")[2], vim.fn.getpos("'>")[2]
  local lines_begin, lines_end = vim.fn.getcurpos()[2], vim.fn.line "v"

  local lines_num
  if lines_begin > lines_end then
    local tem = lines_end
    lines_end = lines_begin
    lines_begin = tem
  end
  lines_num = lines_end - lines_begin + 1
  local lines_str = vim.api.nvim_buf_get_lines(buf, lines_begin - 1, lines_end, false)

  return { lines_begin, lines_end, lines_num, lines_str }
end

local function create_func_def()
  local current_dir = vim.fn.expand "%:p:h"
  current_dir = string.gsub(current_dir, [[\]], "/")
  current_dir = current_dir .. "/"

  local file_name = vim.fn.bufname "%"
  local extend_name
  file_name = file_name:gsub([[\]], "/")
  file_name = file_name:gsub("^%./", "")
  extend_name = file_name
  file_name = file_name:match "(.+)%..+" or file_name
  extend_name = extend_name:match "(%.%w+)"
  local file_type = vim.bo.filetype
  file_name = file_name:gsub(".*/", "")

  if file_type == "cpp" and (extend_name == ".hpp" or extend_name == ".h") then
    local n_info, c_info = get_cpp_header_info(0, file_type)
    local select_lines = get_select_lines(0)
    local funcs = {}

    for _, str in ipairs(select_lines[4]) do
      local namespace = ""
      local class = ""
      local func = ""
      local pattern1 = "^%s*[%w_:]+%s+[%w_:]+%s*%([^()]*%)[^{};]*%s*;$"
      local pattern2 = "%s*[%w_:~]*%s*%([^()]*%)[^{};]*%s*;$"

      if str:find "virtual" then
        str = str:gsub("virtual", "")
      elseif str:find "static" then
        str = str:gsub("static", "")
      end
      local lastSemicolonPos = string.find(str, ";[^;]*$")
      if lastSemicolonPos then
        local substr = string.sub(str, 1, lastSemicolonPos - 1)
        substr = string.gsub(substr, "%s*final%s*$", "")
        substr = string.gsub(substr, "%s*override%s*$", "")
        str = substr .. ";"
      end

      if str:match(pattern1) then
        local ret_type, rest = str:match "(%S+)%s+((%S+)%s*%(%s*(.*)%s*%)%s*%w*);"
        local line = _ + select_lines[1] - 1
        for _, value in ipairs(c_info) do
          if line > value[2] and line < value[3] then
            local tem = value[1]
            class = tem .. "::" .. class
          end
        end
        for _, value in ipairs(n_info) do
          if line > value[2] and line < value[3] then
            local tem = value[1]
            tem = tem:gsub("^%s*", "")
            if namespace == "" then
              namespace = tem .. "::"
            else
              namespace = namespace .. tem .. "::"
            end
            namespace = namespace:gsub("^%s*", "")
          end
        end
        local n_c = namespace .. class:gsub("^%s*", "")
        func = ret_type .. " " .. n_c .. rest .. "{\n\n\n}\n"
        table.insert(funcs, func)
      elseif str:match(pattern2) then
        local line = _ + select_lines[1] - 1
        for _, value in ipairs(c_info) do
          if line > value[2] and line < value[3] then
            local tem = value[1]
            class = tem .. "::" .. class
          end
        end
        func = class .. str:match "^%s*(.-)%s*;?$" .. "{\n\n\n}\n"
        for _, value in ipairs(n_info) do
          if line > value[2] and line < value[3] then
            local tem = value[1]
            tem = tem:gsub("^%s*", "")
            if namespace == "" then
              namespace = tem .. "::"
            else
              namespace = namespace .. tem .. "::"
            end
            namespace = namespace:gsub("^%s*", "")
          end
        end
        func = namespace .. func:match "^%s*(.-)$"
        table.insert(funcs, func)
      end
    end

    if utils.hasfile(current_dir .. file_name .. ".cpp") then
      local file = io.open(current_dir .. file_name .. ".cpp", "a+")
      if not file then
        vim.notify([[can't create func imp!]], error, {
          title = "error",
        })
        return
      end
      for _, v in ipairs(funcs) do
        file:write(v)
      end
      io.close(file)
      vim.notify("in " .. current_dir .. file_name .. ".cpp", info, {
        title = "Create func imp!",
      })
      vim.api.nvim_command("edit " .. current_dir .. file_name .. ".cpp")
    elseif utils.hasfile(current_dir .. file_name .. ".c") then
      local file = io.open(current_dir .. file_name .. ".c", "a+")
      if not file then
        vim.notify([[can't create func imp!]], error, {
          title = "error",
        })
        return
      end
      for _, v in ipairs(funcs) do
        file:write(v)
      end
      io.close(file)
      vim.notify("in " .. current_dir .. file_name .. ".c", info, {
        title = "Create func imp!",
      })
      vim.api.nvim_command("edit " .. current_dir .. file_name .. ".c")
    else
      local file = assert(io.open(current_dir .. file_name .. ".cpp", "w+"), "Open file error!")
      if not file then
        vim.notify([[can't create func imp!]], error, {
          title = "error",
        })
        return
      end
      file:write([[#include "]] .. file_name .. extend_name .. [["]] .. "\n\n\n\n")
      for _, v in ipairs(funcs) do
        file:write(v)
      end
      io.close(file)
      vim.notify("in " .. current_dir .. file_name .. ".cpp", info, {
        title = "Create imp!",
      })
      vim.api.nvim_command("edit " .. current_dir .. file_name .. ".cpp")
    end
  else
    utils.clear_cmd()
  end
end

-- =====================================================================================

return {
  create_file = create_file,
  create_func_def = create_func_def,
}
