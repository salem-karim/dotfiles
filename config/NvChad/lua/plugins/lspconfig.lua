return {
  "neovim/nvim-lspconfig",
  event = "User FilePost",
  config = function()
    dofile(vim.g.base46_cache .. "lsp")
    require("nvchad.lsp").diagnostic_config()

    -- Disable virtual_text since it's redundant due to lsp_lines.
    vim.diagnostic.config {
      virtual_text = false,
    }

    local lspconfig = require "lspconfig"
    local nvlsp = require "configs.lsp"

    local servers = {
      "vimls",
      "gdscript",
      "bashls",
      "html",
      "cssls",
      "ts_ls",
      "biome",
      "cmake",
      "pyright",
      "gopls",
      "zls",
      "ruff",
    }

    -- lsps with default config
    for _, lsp in ipairs(servers) do
      lspconfig[lsp].setup {
        on_attach = nvlsp.on_attach,
        on_init = nvlsp.on_init,
        capabilities = nvlsp.capabilities,
      }
    end

    lspconfig.lemminx.setup {
      -- cmd = nvlsp.lemminx_cmd,
      cmd = { "lemminx-maven" },
      on_attach = nvlsp.on_attach,
      capabilities = nvlsp.capabilities,
      on_init = nvlsp.on_init,
    }

    lspconfig.rust_analyzer.setup {
      cmd = { "/home/karim/.nix-profile/bin/rust-analyzer" },
      on_attach = nvlsp.on_attach,
      capabilities = nvlsp.capabilities,
      on_init = nvlsp.on_init,
    }

    lspconfig.clangd.setup {
      cmd = { "/home/karim/.nix-profile/bin/clangd" },
      on_attach = nvlsp.on_attach,
      capabilities = nvlsp.capabilities,
      on_init = nvlsp.on_init,
    }

    lspconfig.nixd.setup {
      on_attach = nvlsp.on_attach,
      on_init = nvlsp.on_init,
      capabilities = nvlsp.capabilities,
      cmd = { "nixd" },
      settings = {
        nixd = {
          nixpkgs = {
            expr = "import <nixpkgs-unstable> { }",
          },
          formatting = {
            command = { "nixfmt" },
          },
        },
      },
    }

    lspconfig.lua_ls.setup {
      on_attach = nvlsp.on_attach,
      on_init = nvlsp.on_init,
      capabilities = nvlsp.capabilities,
      settings = {
        Lua = {
          diagnostics = {
            globals = { "vim" },
          },
          workspace = {
            library = {
              vim.fn.expand "$VIMRUNTIME/lua",
              vim.fn.expand "$VIMRUNTIME/lua/vim/lsp",
              vim.fn.stdpath "data" .. "/lazy/ui/nvchad_types",
              vim.fn.stdpath "data" .. "/lazy/lazy.nvim/lua/lazy",
              "${3rd}/luv/library",
            },
            maxPreload = 100000,
            preloadFileSize = 10000,
          },
        },
      },
    }

    lspconfig.omnisharp.setup {
      on_attach = nvlsp.on_attach,
      on_init = nvlsp.on_init,
      capabilities = nvlsp.capabilities,
      cmd = { "dotnet", "/home/karim/.local/share/nvim/mason/packages/omnisharp/libexec/OmniSharp.dll" },
      handlers = {
        ["textDocument/definition"] = function(...)
          return require("omnisharp_extended").handler(...)
        end,
      },
      filetype = { "cs", "vb", "fsharp" },
      settings = {
        FormattingOptions = {
          EnableEditorConfigSupport = true,
          OrganizeImports = true,
        },
        MsBuild = {
          LoadProjectsOnDemand = true,
        },
        RoslynExtensionsOptions = {
          EnableAnalyzersSupport = true,
          EnableImportCompletion = true,
          AnalyzeOpenDocumentsOnly = true,
        },
        Sdk = {
          IncludePrereleases = true,
        },
      },
    }
  end,
}
