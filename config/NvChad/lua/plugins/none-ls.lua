return {
  "nvimtools/none-ls.nvim",
  event = "VeryLazy",
  dependencies = {
    "nvimtools/none-ls-extras.nvim",
  },
  opts = function()
    local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
    local null_ls = require "null-ls"

    return {
      sources = {
        null_ls.builtins.formatting.biome,
        null_ls.builtins.diagnostics.codespell,
        -- null_ls.builtins.diagnostics.mypy,
        null_ls.builtins.diagnostics.cmake_lint,
        null_ls.builtins.diagnostics.cppcheck.with {
          extra_args = { "--std" },
        },
        null_ls.builtins.diagnostics.gdlint,
        null_ls.builtins.formatting.gdformat.with {
          extra_args = { "--use-spaces=2" },
        },
        null_ls.builtins.diagnostics.sqlfluff.with {
          extra_args = { "--dialect", "postgres" },
        },
      },
      on_attach = function(client, bufnr)
        if client.supports_method "textDocument/formatting" then
          vim.api.nvim_clear_autocmds {
            group = augroup,
            buffer = bufnr,
          }
          vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup,
            buffer = bufnr,
            callback = function()
              vim.lsp.buf.format { bufnr = bufnr }
            end,
          })
        end
      end,
    }
  end,
}
