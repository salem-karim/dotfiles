return {

  { "nvim-pack/nvim-spectre" },
  { "akinsho/toggleterm.nvim", version = "*", config = true },
  { "Hoffs/omnisharp-extended-lsp.nvim", lazy = true },

  {
    "lewis6991/gitsigns.nvim",
    event = "User FilePost",
    opts = function()
      dofile(vim.g.base46_cache .. "git")
      return {
        signs = {
          delete = { text = "󰍵" },
          changedelete = { text = "󱕖" },
        },
      }
    end,
  },

  {
    "windwp/nvim-ts-autotag",
    ft = { "html", "javascript", "typescript" },
    config = function()
      require("nvim-ts-autotag").setup()
    end,
  },

  {
    "folke/trouble.nvim",
    cmd = { "TroubleToggle", "Trouble" },
    config = function()
      dofile(vim.g.base46_cache .. "trouble")
      local opts = { use_diagnostic_signs = true }
      require("trouble").setup(opts)
    end,
  },

  {
    "RRethy/vim-illuminate",
    lazy = false,
    config = function()
      require("illuminate").configure {
        min_count_to_highlight = 2,
      }
    end,
  },

  {
    "christoomey/vim-tmux-navigator",
    lazy = false,
    cmd = {
      "TmuxNavigateLeft",
      "TmuxNavigateDown",
      "TmuxNavigateUp",
      "TmuxNavigateRight",
      "TmuxNavigatePrevious",
    },
  },

  {
    "p00f/clangd_extensions.nvim",
    lazy = false,
    ft = { "c", "cpp" },
    opts = {},
  },

  {
    "LunarVim/bigfile.nvim",
    event = "BufReadPre",
    opts = {
      filesize = 4, -- minimum size of file for plugin to kick in. (Mib)
    },
  },

  {
    "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
    lazy = false,
    config = function()
      require("lsp_lines").setup()
    end,
  },
}
