local sql_ft = { "sql", "mysql", "plsql" }

return {
  {
    "tpope/vim-dadbod",
    cmd = "DB",
  },
  {
    "kristijanhusak/vim-dadbod-completion",
    dependencies = "vim-dadbod",
    ft = sql_ft,
    init = function()
      vim.api.nvim_create_autocmd("FileType", {
        pattern = sql_ft,
        callback = function()
          local cmp = require("cmp")
          if not cmp then
            return
          end
          -- Get current sources or create empty table if none exist
          local config = cmp.get_config()
          local sources = config and config.sources or {}
          -- Convert sources to the format we need
          local formatted_sources = {}
          for _, source in ipairs(sources) do
            table.insert(formatted_sources, { name = source.name })
          end
          -- Add vim-dadbod-completion source
          table.insert(formatted_sources, { name = "vim-dadbod-completion" })

          -- Update sources for the current buffer
          cmp.setup.buffer({ sources = formatted_sources })
        end,
      })
    end,
  },
  {
    "kristijanhusak/vim-dadbod-ui",
    cmd = { "DBUI", "DBUIToggle", "DBUIAddConnection", "DBUIFindBuffer" },
    dependencies = "vim-dadbod",
    init = function()
      local data_path = vim.fn.stdpath("data")

      -- Database UI configuration
      vim.g.db_ui_auto_execute_table_helpers = 1
      vim.g.db_ui_save_location = data_path .. "/dadbod_ui"
      vim.g.db_ui_show_database_icon = true
      vim.g.db_ui_tmp_query_location = data_path .. "/dadbod_ui/tmp"
      vim.g.db_ui_use_nerd_fonts = true
      vim.g.db_ui_use_nvim_notify = true
    end,
  }
}
