return {
  {
    "nvim-treesitter/nvim-treesitter-context",
    cmd = {
      "TSContextEnable",
      "TSContextDisable",
      "TSContextToggle",
    },
    config = function()
      require("treesitter-context").setup {
        max_lines = 3,
      }
      local colors = require "colors"
      local hl = vim.api.nvim_set_hl
      hl(0, "TreesitterContext", { bg = "#40405b" })
      hl(0, "TreesitterContextLineNumber", { fg = colors.mocha.blue })
      hl(0, "TreesitterContextBottom", { underline = true, sp = colors.mocha.sky })
      hl(0, "TreesitterContextLineNumberBottom", { underline = false })
    end,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    event = { "BufReadPost", "BufNewFile" },
    cmd = { "TSInstall", "TSBufEnable", "TSBufDisable", "TSModuleInfo" },
    build = ":TSUpdate",
    opts = function()
      pcall(function()
        dofile(vim.g.base46_cache .. "syntax")
        dofile(vim.g.base46_cache .. "treesitter")
      end)
      return {
        highlight = {
          enable = true,
          use_languagetree = true,
        },

        indent = { enable = true },
        ensure_installed = {
          -- vim
          "printf",
          "lua",
          "luadoc",
          "vim",
          "vimdoc",
          -- WebDev
          "html",
          "css",
          "sql",
          "xml",
          "javascript",
          "typescript",
          -- C/C++/C#
          "c",
          "cpp",
          "c_sharp",
          -- build tools
          "cmake",
          "make",
          "bash",
          "dockerfile",
          -- other
          "haskell",
          "python",
          "java",
          "r",
          -- new langs
          "zig",
          "rust",
          "go",
          -- Godot
          "gdscript",
          "gdshader",
        },
      }
    end,
    config = function(_, opts)
      require("nvim-treesitter.configs").setup(opts)
    end,
  },
}
