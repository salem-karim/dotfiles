return {
  "aurum77/live-server.nvim",
  config = function()
    local status_ok, live_server = pcall(require, "live_server")
    if not status_ok then
      return
    end

    live_server.setup {
      port = 5500,
      browser_command = "zen", -- Command or executable path
      quiet = false,
      no_css_inject = true,
      install_path = os.getenv "HOME" .. "/.nix-profile/bin/",
    }
  end,
  cmd = {
    "LiveServer",
    "LiveServerStart",
    "LiveServerStop",
  },
  build = function()
    require("liver_server.util").install()
  end,
}
