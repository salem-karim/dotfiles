return {
  "williamboman/mason.nvim",
  cmd = { "Mason", "MasonInstall", "MasonUpdate" },
  opts = function()
    dofile(vim.g.base46_cache .. "mason")
    return {
      ensure_installed = {
        "luacheck",
        -- Web Development
        "html-lsp",
        "css-lsp",
        "typescript-language-server",
        "eslint-lsp",
        "json-lsp",
        "js-debug-adapter",
        -- "angular-language-server",
        --C/C++
        "codelldb",
        "cmakelang",
        -- Java
        "jdtls",
        "java-test",
        "java-debug-adapter",
        "vscode-java-decompiler",
        -- Other
        "go-debug-adapter",
        "debugpy",
        "dockerfile-language-server",
      },
      PATH = "skip",

      ui = {
        icons = {
          package_pending = " ",
          package_installed = " ",
          package_uninstalled = " ",
        },
      },

      max_concurrent_installers = 10,
    }
  end,
}
