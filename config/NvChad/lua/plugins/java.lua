return {

  { "mfussenegger/nvim-jdtls" },

  {
    "JavaHello/spring-boot.nvim",
    ft = "java",
    dependencies = {
      "mfussenegger/nvim-jdtls", -- or nvim-java, nvim-lspconfig
    },
    config = function()
      require("spring_boot").setup {}
    end,
  },

  {
    "salem-karim/maven.nvim",
    branch = "newfeat",
    cmd = { "Maven", "MavenExec" },
    dependencies = "nvim-lua/plenary.nvim",
    config = function()
      require("maven").setup {
        executable = "mvn",
        -- maven_central_url = 'https://mvnrepository.com/repos/central', if nil https://central.sonatype.com/
      }
    end,
  },

}
