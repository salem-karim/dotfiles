return {

  {
    "ThePrimeagen/refactoring.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
    },
    lazy = false,
    config = function()
      require("refactoring").setup()
    end,
  },

  {
    "echasnovski/mini.jump",
    lazy = false,
    version = false,
    opts = {},
  },

  {
    "echasnovski/mini.ai",
    lazy = false,
    version = false,
    opts = {},
  },

  {
    "echasnovski/mini.surround",
    lazy = false,
    version = false,
    config = function()
      require("mini.surround").setup {
        mappings = {
          add = "sa",
          delete = "sd",
          find = "sf",
          find_left = "sF",
          highlight = "sh",
          replace = "sr",
          update_n_lines = "sn",

          suffix_last = "l",
          suffix_next = "n",
        },
      }
    end,
  },

  {
    "mg979/vim-visual-multi",
    lazy = false,
    branch = "master",
    init = function()
      vim.g.VM_default_mappings = 0
      vim.g.VM_set_statusline = 0
      vim.g.VM_silent_exit = 1
      vim.g.VM_quit_after_leaving_insert_mode = 1
      vim.g.VM_show_warnings = 0
      vim.g.VM_highlight_matches = ""
      vim.g.VM_maps = {
        ["Undo"] = "u",
        ["Redo"] = "<C-r>",
        ["Find Under"] = "<C-d>",
        ["Skip Region"] = "<C-s>",
        ["Select h"] = "<C-S-h>",
        ["Select l"] = "<C-S-l>",
        ["Add Cursor Up"] = "<C-S-k>",
        ["Add Cursor Down"] = "<C-S-j>",
        ["Mouse Cursor"] = "<C-LeftMouse>",
        ["Mouse Column"] = "<C-RightMouse>",
      }
    end,
  },
}
