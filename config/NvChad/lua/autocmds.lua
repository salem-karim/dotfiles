local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

-- user event that loads after UIEnter + only if file buf is there
autocmd({ "UIEnter", "BufReadPost", "BufNewFile" }, {
  group = vim.api.nvim_create_augroup("NvFilePost", { clear = true }),
  callback = function(args)
    local file = vim.api.nvim_buf_get_name(args.buf)
    local buftype = vim.api.nvim_get_option_value("buftype", { buf = args.buf })

    if not vim.g.ui_entered and args.event == "UIEnter" then
      vim.g.ui_entered = true
    end

    if file ~= "" and buftype ~= "nofile" and vim.g.ui_entered then
      vim.api.nvim_exec_autocmds("User", { pattern = "FilePost", modeline = false })
      vim.api.nvim_del_augroup_by_name "NvFilePost"

      vim.schedule(function()
        vim.api.nvim_exec_autocmds("FileType", {})

        if vim.g.editorconfig then
          require("editorconfig").config(args.buf)
        end
      end)
    end
  end,
})

autocmd("TextYankPost", {
  desc = "Highlight when yanking (copying) text",
  group = vim.api.nvim_create_augroup("kickstart-highlight-yank", { clear = true }),
  callback = function()
    vim.highlight.on_yank()
  end,
})

vim.api.nvim_create_augroup("Indentation", { clear = true })
vim.cmd "TSContextEnable"

local macro_group = augroup("MacroRecording", { clear = true })
autocmd("RecordingLeave", {
  group = macro_group,
  callback = function()
    -- Display a message when macro recording stops
    print "Macro recording stopped"
  end,
})

-- Define a table of excluded filetypes
local excluded_filetypes = {
  "cs",
  "sql",
  "zig",
}

-- Function to check if the filetype is excluded
local function is_excluded_filetype(ft)
  for _, excluded in ipairs(excluded_filetypes) do
    if ft == excluded then
      return true
    end
  end
  return false
end

-- Set up an autocmd group to handle indentation
augroup("Indentation", { clear = true })
vim.cmd "TSContextEnable"

-- Set default indentation for all files to 2 spaces, unless excluded
autocmd("FileType", {
  group = "Indentation",
  pattern = "*",
  callback = function()
    if not is_excluded_filetype(vim.bo.filetype) then
      vim.opt_local.expandtab = true
      vim.opt_local.smartindent = true
      vim.opt_local.tabstop = 2
      vim.opt_local.shiftwidth = 2
      vim.opt_local.softtabstop = 2
    end
  end,
})
