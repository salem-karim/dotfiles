local utils = require "nvchad.stl.utils"
local sep_style = "block"
local sep_icons = utils.separators
local separators = (type(sep_style) == "table" and sep_style) or sep_icons[sep_style]

local sep_l = separators["left"]
local sep_r = separators["right"]

vim.api.nvim_set_hl(0, 'St_GitChanged', { bg = '#2d2c3c', fg = '#f8bd96', })
vim.api.nvim_set_hl(0, 'St_GitRemoved', { bg = '#2d2c3c', fg = '#f38ba8', })

local file = function()
  local icon = "󰈚"
  local hl_group = ""
  local path = vim.api.nvim_buf_get_name(utils.stbufnr())
  local name = (path == "" and "Empty") or path:match "([^/\\]+)[/\\]*$"

  if name ~= "Empty" then
    local devicons_present, devicons = pcall(require, "nvim-web-devicons")

    if devicons_present then
      local ft_icon, ft_hl = devicons.get_icon(name) -- Second return value is the highlight group
      icon = (ft_icon ~= nil and ft_icon) or icon
      hl_group = ft_hl or ""                         -- Store highlight group
    end
  end

  return { icon, name, hl_group }
end

local function update_custom_highlight()
  local icon_hl = file()[3]                                                -- Get the icon's highlight group
  local icon_fg = vim.api.nvim_get_hl(0, { name = icon_hl }).fg or "white" -- Default to white if no fg
  local bg_color = "#2d2c3c"                                               -- Fixed background color

  -- Define a custom highlight group
  vim.api.nvim_set_hl(0, 'CustomIconHighlight', {
    fg = icon_fg,
    bg = bg_color,
  })

  -- Force a statusline refresh
  vim.cmd("redrawstatus")
end

-- Update highlight on Neovim startup
vim.api.nvim_create_autocmd('VimEnter', {
  callback = update_custom_highlight
})

-- Update highlight when switching buffers
vim.api.nvim_create_autocmd('BufEnter', {
  callback = update_custom_highlight
})

local stbufnr = function()
  return vim.api.nvim_win_get_buf(vim.g.statusline_winid or 0)
end

local function mygit()
  if not vim.b[stbufnr()].gitsigns_head or vim.b[stbufnr()].gitsigns_git_status then
    return ""
  end

  local git_status = vim.b[stbufnr()].gitsigns_status_dict

  local added = (git_status.added and git_status.added ~= 0) and ("  " .. git_status.added) or ""
  local changed = (git_status.changed and git_status.changed ~= 0) and ("  " .. git_status.changed) or ""
  local removed = (git_status.removed and git_status.removed ~= 0) and ("  " .. git_status.removed) or ""
  local branch_name = "%#St_GitRemoved#" .. " " .. "%#St_file#" .. git_status.head

  return " " ..
      branch_name .. "%#St_pos_text#" .. added .. "%#St_GitChanged#" .. changed .. "%#St_GitRemoved#" .. removed
end

local statusline = {
  -- theme = "vscode_colored",
  -- theme = "minimal",
  separator_style = sep_style,
  order = { "mymode", "mygit", "myfile", "%=", "lsp_msg", "%=", "diagnostics", "lsp", "mycursor", "cwd" },
  modules = {
    mycursor = function()
      return "%#St_pos_sep#" .. sep_l .. "%#St_pos_icon# %#St_pos_text# %l:%c "
    end,
    mymode = function()
      if not utils.is_activewin() then
        return ""
      end

      local modes = utils.modes

      local m = vim.api.nvim_get_mode().mode

      local current_mode = "%#St_" .. modes[m][2] .. "Mode# " .. modes[m][1]
      local mode_sep1 = "%#St_" .. modes[m][2] .. "ModeSep#" .. sep_r
      return current_mode .. mode_sep1 .. "%#ST_EmptySpace#"
    end,
    mygit = function()
      return mygit()
    end,
    myfile = function()
      local x = file()
      local name = x[2] .. (sep_style == "default" and " " or "")
      return "%#CustomIconHighlight# " .. x[1] .. "%#St_file# " .. name .. "%#St_file_sep#" .. sep_r
    end
  }
}

return statusline
