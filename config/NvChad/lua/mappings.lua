local map = vim.keymap.set

-- General
map("n", "<Esc>", "<cmd>noh<CR>", { desc = "general clear highlights" })
map("n", "<C-q>", ":q<CR>", { desc = "Quit", silent = true })
map({ "n", "i", "v" }, "<C-s>", "<cmd>w<cr>")
map("n", "<C-c>", "<cmd>%y+<CR>", { desc = "general copy whole file" })
map({ "n", "i" }, "<C-a>", "ggVG", { desc = "Select All" })
map("v", "<C-c>", '"+y', { desc = "Copy to System Clipboard" })
map({ "n", "v" }, '<leader>"', function()
  Snacks.picker.registers()
end, { desc = "Pick Register" })
-- Remap 'm' to 'q' for macro recording
vim.cmd "nnoremap m q"
vim.cmd "nnoremap q <Nop>"

-- Convenience
map("n", ";", ":", { desc = "CMD enter command mode" })
map({ "n", "v" }, "<leader>P", '"1p', { desc = "paste last yanked" })
map("n", "<leader>ll", ":Lazy<cr>", { desc = "Lazy", silent = true })

-- tabufline
map("n", "<tab>", function()
  require("nvchad.tabufline").next()
end, { desc = "buffer goto next" })

map("n", "<S-tab>", function()
  require("nvchad.tabufline").prev()
end, { desc = "buffer goto prev" })

map("n", "<leader>x", function()
  require("nvchad.tabufline").close_buffer()
end, { desc = "buffer close" })

-- Movement
map({ "n", "v" }, "H", "^", { desc = "Move to beginning of context" })
map({ "n", "v" }, "L", "$", { desc = "Move to end of context" })
map("i", "<A-a>", "<ESC>^i", { desc = "move beginning of line" })
map("i", "<A-d>", "<End>", { desc = "move end of line" })
map("i", "<A-j>", "<Down>", { desc = "move down" })
map("i", "<A-k>", "<Up>", { desc = "move up" })
map("n", "<leader>gc", function()
  require("treesitter-context").go_to_context(vim.v.count1)
end, { desc = "Go to context", silent = true })

-- Live Server&Markdown & Database
map("n", "<leader>ls", ":LiveServer<CR>", { desc = "start/stop Live Server", silent = true })
map("n", "<leader>tdb", "<cmd>DBUIToggle<cr>", { desc = "Toggle Database UI", silent = true })
map("n", "<leader>tm", ":RenderMarkdown toggle<CR>", { desc = "Toggle Markdown Render", silent = true })

-- Trouble
map("n", "<leader>dt", "<cmd>Trouble diagnostics toggle<cr>", { desc = "Toggle Diagnostics", silent = true })
map("n", "<leader>qf", "<cmd>Trouble qflist toggle<cr>", { desc = "Toggle Quickfixes", silent = true })
map("n", "<leader>st", function()
  Snacks.picker.todo_comments()
end, { desc = "Todo" })

-- Spectre
map("n", "<leader>sa", '<cmd>lua require("spectre").toggle()<CR>', { desc = "Search all files" })
map("v", "<leader>sw", '<esc><cmd>lua require("spectre").open_visual()<CR>', { desc = "Search current word" })
map(
  "n",
  "<leader>sw",
  '<cmd>lua require("spectre").open_visual({select_word=true})<CR>',
  { desc = "Search current word" }
)

map(
  "n",
  "<leader>sf",
  '<cmd>lua require("spectre").open_file_search({select_word=true})<CR>',
  { desc = "Search on current file" }
)

-- Tmux navigator mappings
map({ "n", "v", "x", "i" }, "<C-h>", "<cmd>TmuxNavigateLeft<CR>", { desc = "Window left", silent = true })
map({ "n", "v", "x", "i" }, "<C-j>", "<cmd>TmuxNavigateDown<CR>", { desc = "Window down", silent = true })
map({ "n", "v", "x", "i" }, "<C-k>", "<cmd>TmuxNavigateUp<CR>", { desc = "Window up", silent = true })
map({ "n", "v", "x", "i" }, "<C-l>", "<cmd>TmuxNavigateRight<CR>", { desc = "Window right", silent = true })

-- Refactor
local function ref(text)
  return { desc = "Refactor: " .. text }
end

map("x", "<leader>re", ":Refactor extract ", ref "extract")
map("x", "<leader>rf", ":Refactor extract_to_file ", ref "extract to file")
map("x", "<leader>rv", ":Refactor extract_var ", ref "extract variable")
map({ "n", "x" }, "<leader>ri", ":Refactor inline_var<CR>", ref "inline variable")
map("n", "<leader>rI", ":Refactor inline_func<CR>", ref "inlince function")
map("n", "<leader>rb", ":Refactor extract_block<CR>", ref "extract block")
map("n", "<leader>rbf", ":Refactor extract_block_to_file<CR>", ref "extract block to file")
